Things to do:
-------------

-add a 'file type' field to photos so that they can be stored with proper file extensions

-put more magic numbers into the bootstrap.php file: things like the number of photos per page in a 
media gallery, the Space Jam number (eg. Space Jam 4, 5, etc.), etc.

-can probably combine create and edit functions in the media controller/views

-condense the add function in media so that it uses javascript to script the second stage

-fix the validation rules in /models/media.php to say that if type = video, code != null and also to
show that type must be either video or photo

-maybe (and I stress maybe...) put in a way to control the order that media is displayed in a gallery

-put in navigation and back links at the top of gallery pages as well as the bottom

-should I change the photo/video displayed on the main multimedia page from 'latest' to 'featured'?

-need to add a 'missing news story' page to put up instead of page just showing '-;'

-show galleries list on the view pages

-add controls to add new admins/members and let them change their passwords

-centralize data in a data helper: sponsors, merit badges, etc.

-Sponsors page:
	-sidebar is list of sponsors who's links are javascript actions to:...
	-...dispay one main sponsor in the content area w/ picture and link to site 
	
-system to look up meritbadges onsite