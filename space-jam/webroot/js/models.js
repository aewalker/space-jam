//Block class
function Block(time) {
	this.time = time;
	this.badges = new Array();
}

//creates a copy of this block but removes all badges from the new one
Block.prototype.clone = function() {
	return new Block(this.time);
};

//returns this block's time
Block.prototype.getBlockTime = function() {
	return this.time;
};

//returns this block's badges
Block.prototype.getBadges = function() {
	return this.badges.valueOf();
};

//adds the given badge to this block
Block.prototype.addBadge = function(badge) {
	this.badges.push(badge);
};

//removes the given badge from this block (does nothing if this block does not have the given badge)
Block.prototype.removeBadge = function(badge) {
	var index = this.badges.indexOf(badge);
	if (index != -1) {
		this.badges.splice(index, 1);
	}
};

/*************************************************************************************************/

//Badge class
function Badge(id, name, times, types, spots, cost, note) {
	this.name = name;
    times.sort(function(a, b) {
        return a - b;
    });
	this.times = times;
	this.types = types;
	this.spots = spots;
	this.cost = cost;
	this.id = parseInt(id);
    this.note = note;
}

//returns the starting time
Badge.prototype.getFirstBlock = function() {
	return this.times[0];
};

//returns the last time
Badge.prototype.getLastBlock = function() {
	return this.times[this.times.length - 1];
};

//returns true if this badge has a nonzero cost
Badge.prototype.hasCost = function() {
    if (this.cost === 0) {
        return false;
    }
    return true;
};

//get cost for the billing
Badge.prototype.getCost = function() {
	return this.cost;
};

//gets the number of spots left in this badge's class
Badge.prototype.getSpots = function() {
	return this.spots;
};

//gets this badge's name
Badge.prototype.getName = function() {
	return this.name;
};

//gets this badge's type
Badge.prototype.getTypes = function() {
	return this.types;
};

//ture if this badge has a custom note
Badge.prototype.hasNote = function() {
    if (this.note === null) {
        return false;
    }
    return true;
};

//gets this badge's note
Badge.prototype.getNote = function() {
    return this.note;
};

//gets this badge's id
Badge.prototype.getId = function() {
    return this.id;
};

//add this badge to the given blocks.  Returns false if there is a schedule conflict or not enough spots.
Badge.prototype.add = function(blocks) {
	if (this.spots < 1) {
		return false;
	}
	var toAddTo = new Array();
	var i;
	var j;
	for (i = 0; i < this.times.length; i++) {
		for (j = 0; j < blocks.length; j++) {
			if (blocks[j].getBlockTime() == this.times[i]) {
				if (blocks[j].getBadges().length >= 1) {
					//i is outer loop, time that this badge is scheduled, j is the inter loop and it is blocks of a scouts schedule we are trying to add to..
					return false;
				}
				else {
					toAddTo.push(blocks[j]);
					//adds to a queue of blocks we are tentetively adding this badge too
					break;
				}
			}
		}
	} //we have found that all the needed blocks are free and are now committing it to the schedule
	//add ajax code here validate it works on the server (no one grabbed the last sport)  If this fails we need to reload spots data for everything.
	for (i = 0; i < toAddTo.length; i++) {
		toAddTo[i].addBadge(this);
	}
	this.spots--; //decrements the on page number of spots left
	return true;
};

//adds this badge to the given blocks and ignores schedule conflicts
Badge.prototype.addIgnoreConflicts = function(blocks) {  //this is how we build the table of badges initially
	var i;
	var j;
	for (i = 0; i < this.times.length; i++) {
		for (j = 0; j < blocks.length; j++) {
			if (blocks[j].getBlockTime() == this.times[i]) {
				blocks[j].addBadge(this);
				break;
			}
		}
	}
};

//removes this badge from the given blocks (does nothing if the given blocks do not contain this badge)
Badge.prototype.drop = function(blocks) {
	var i;
	for (i = 0; i < blocks.length; i++) {
		blocks[i].removeBadge(this);
	}
	this.spots++;
};

/*************************************************************************************************/

//Scout class.  Since this constructor uses pseudo-SJAX, you can not use the
//resulting object immediately after it's created.  Also, you should check that
//the scout is valid (use isValid()) before using it.  onCreated is a callback
//method that will be called only after the scout is successfully created.
function Scout(name, type, blocks, onCreated, existingId) {
    this.valid = false;
    var that = this;
    var makeScout = function (id, errmsg) {
    	if (id == -1) {
            error(false, "Could not create new scout:\n" + errmsg);
        }
        that.valid = true;
	    that.name = name;
	    that.type = type;
        that.id = id;
	    that.blocks = new Array();
	    var i;
	    for (i = 0; i < blocks.length; i++) {
		    that.blocks.push(blocks[i].clone()); //gives a list of blocks for scout to fill with a schedule of badges
	    }
	    that.badges = new Array();
	    that.cost = 0;
        onCreated(that);
    };
    if (existingId === undefined) {
	    get_new_scout(name, type, makeScout);
    } else {
    	makeScout(existingId, "");
    }
}

//check to see if the scout is valid
Scout.prototype.isValid = function() {
    return this.valid;
};

//clones this scout (and his schedule), but gives him a new name
Scout.prototype.clone = function(name, onCreated) {
    var that = this;
	var newScout = new Scout(name, this.type, this.blocks, function(scout) {
        var i;
	    for (i = 0; i < that.badges.length; i++) {
		    newScout.addBadge(that.badges[i]);
	    }
        onCreated(newScout);
    });
	return newScout;
};

//gets the scout's name
Scout.prototype.getName = function() {
	return this.name;
};

//changes the scout's name to the given value
Scout.prototype.changeName = function(name, cont) {
    var that = this;
    change_name(this.id, name, function(msg) {
        if (msg.match(/\S/)) {
            error(false, msg);
        } else {
        	that.name = name;
        }
        cont();
    });
};

//tries to sign this scout up for the given badge; returns false if there is a schedule problem
Scout.prototype.addBadge = function(badge) {
	if (this.typeMatches(badge.getTypes()) === false) {
		return false;
	}
	if (badge.add(this.blocks) === true) {
        enroll_scout(this.id, badge.getId(), function() { });
		this.badges.push(badge);
		this.cost += badge.getCost();
		return true;
	}
	return false;
};

//add the given badge to this scout, don't tell the server, and freak out if it doesn't work
Scout.prototype.hasBadge = function(badge) {
	if (badge.add(this.blocks) !== true) {
		error(true, "Scout.hasBadge: add failed for: badge = " + badge.getId() + ", scout = " + this.id);
	} else {
		this.badges.push(badge);
		this.cost += badge.getCost();
	}
};

//true if the given type is compatible with this scout, false if not
Scout.prototype.typeMatches = function(types) {
    var i;
    for (i = 0; i < types.length; i++) {
	    if (this.type == types[i]) {
            return true;
        }
    }
    return false;
};

//drops the given badge from this scout's schedule (does nothing if this scout is not in that badge)
Scout.prototype.dropBadge = function(badge, cont) {
	var index = this.badges.indexOf(badge);
	if (index != -1) {
		this.badges.splice(index, 1);
		badge.drop(this.blocks);
		this.cost -= badge.getCost();
        unenroll_scout(this.id, badge.getId(), cont);
	}
};

//returns this scout's badges
Scout.prototype.getBadges = function() {
	return this.badges.valueOf();
};

//gets the cost of all of this scout's badges
Scout.prototype.getCost = function() {
	return this.cost;
};

//gets this scout's id
Scout.prototype.getId = function() {
	return this.id;
};

//get this scout's type
Scout.prototype.getType = function() {
	return this.type;
};

//converts the scout's type to an apropriate abriviation
Scout.prototype.typeAbriv = function() {
	switch (this.type) {
	case "Scout":
		return "S";
	case "Webelo":
		return "W";
	case "Weblo Leader":
		return "WL";
	case "Scouter":
		return "SL";
	default:
		return this.type;
	}
};

//"destroys" this scout - after this is called, DO NOT use the scout again
Scout.prototype.destroy = function() {
    if (this.valid === false) {
        error(true, "atempted to destroy invalid scout");
    }
	var i;
	for (i = 0; i < this.badges.length; i++) {
		this.badges[i].spots++; //TODO fix this
	}
	delete_scout(this.id);
};
