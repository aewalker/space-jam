//creates a badge object from a array in the JSON
function makeBadge(data) {
	var times = new Array();
	var start = timeConvert(data.hour);
	for (var i = 0; i < lengthConvert(data.duration); i++) {
		times.push(start + (0.5 * i));
	}
	var cost = 0;
	if (!isNaN(parseFloat(data.fee))) {
		cost = parseFloat(data.fee);
	}
    var types = data.cat.split(/\s/);
	return new Badge(data.id, data.name, times, types, parseFloat(data.size), cost, data.note);
}

//take a time in Mil turn it into the block system that starts at 0 and goes to 23.5 or 30 minutes per block
function timeConvert(oldTime) {
	var newTime = Math.round(oldTime / 100);
	if (oldTime % 100 == 30) {
		newTime += 0.5;
	}
	return newTime;
}

//take a block time (30 with .5= 30 minutes) and convert it to a nice human readable time.
function timeReverse(newTime) {
	if (newTime >= 13) {
		newTime -= 12;
	}
	var oldTime = Math.floor(newTime);
	oldTime = oldTime + ":" + ((newTime - oldTime) * 6) + "0";
	return oldTime;
}

//converts the length given to an internal value type
function lengthConvert(length) {
	return timeConvert(length) / 0.5;
}

//the init function for registration
startup_funcs.push(function () {
	inactive();
	get_all_badges(function (all_badges) {
		var first = 24;
		var last = 0;
	    var global_badges = new Array();
	    var global_blocks = new Array();
		var i;
		for (i = 0; i < all_badges.length; i++) {
			var thisStart = timeConvert(all_badges[i].hour);
			var thisEnd = thisStart + lengthConvert(all_badges[i].duration) / 2;
			if (thisEnd == thisStart) {
				alert("Invalid Badge:\n\"" + all_badges[i].name + "\"");
				return false;
			}
			if (thisStart < first) {
				first = thisStart;
			}
			if (thisEnd > last) {
				last = thisEnd;
			}
			global_badges.push(makeBadge(all_badges[i]));
		}
		for (i = 0; i * 0.5 + first < last; i++) {
			global_blocks.push(new Block(first + (0.5 * i))); //start at zero make a new block starting at first and going to last
		}
		for (i = 0; i < global_badges.length; i++) {
			global_badges[i].addIgnoreConflicts(global_blocks); //this line builds all the data for the table (doesn't do the presentaiton)
		}
		get_all_scouts(function (all_scouts) {
		    var global_scouts = new Array();
			var i;
			var numScouts = 0;
			for (i = 0; i < all_scouts.length; i++) {
				get_scout(all_scouts[i], function(scout_info) {
					var scout = new Scout(
							scout_info.name,
							scout_info.typeofuser,
							global_blocks,
							function () { },
							scout_info.id);
					var j;
					if (scout_info.badges !== undefined) {
						for (j = 0; j < scout_info.badges.length; j++) {
							var badge;
							var k;
							for (k = 0; k < global_badges.length; k++) {
								if (global_badges[k].getId() == scout_info.badges[j]) {
									badge = global_badges[k];
									break;
								}
							}
							if (badge === undefined) {
								error(true, "Unkown badge: " + scout_info.badges[j]);
							}
							scout.hasBadge(badge);
						}
					}
					global_scouts.push(scout);
					numScouts++;
					if (numScouts == all_scouts.length) {
						new Page(global_blocks, global_badges, global_scouts);
				        active();
					}
				});
			}
		});
	});
});

//gets all the badges
function get_all_badges(cont) {
	jQuery.get("/badges/getAllBadges/", null, function (data, textStatus, jqXHR) {
		cont(data);
	}, "json");
	//'id', 'name', 'cat', 'loc', 'section', 'hour', 'duration', 'size', 'restrictions', 'fee'
}

//gets all the currently registered scouts
function get_all_scouts(cont) {
	jQuery.get("/users/getAllScouts/", null, function (data, textStatus, jqXHR) {
		if (typeof(data) === 'string') {
			error(true, data);
		} else {
			cont(data);
		}
	}, "json");
}

//creates a new scout (on the server) and returns his id
function get_new_scout(name, type, cont) {
	inactive();
    jQuery.get("/users/addScout/" + name + "/" + type + "/", null, function (data, textStatus, jqXHR) {
    	active();
        var rows = data.split("\n");
        if (rows[0] == -1) {
            cont(-1, rows[1]);
        } else {
            cont(parseInt(rows[0]), "");
        }
    });
}

//gets info on a particular scout
function get_scout(id, cont) {
	jQuery.get("/users/getScout/" + id + "/", null, function (data, textStatus, jqXHR) {
		if (typeof(data) === 'string') {
			error(true, data);
		} else {
			cont(data);
		}
	}, "json");
}

//deletes a scout on the server
function delete_scout(id) {
    jQuery.get("/users/deleteScout/" + id + "/", null, function (data, textStatus, jqXHR) {
        if (data.match(/\S/)) {
            error(true, "delete_scout: " + data);
        }
    });
}

//enrolls a scout in a badge (on the server)
function enroll_scout(scout_id, badge_id, onSuccess) {
	inactive();
    jQuery.get("/users/addBadge/" + scout_id + "/" + badge_id + "/", null, function (data, textStatus, jqXHR) {
    	active();
        if (data.match(/\S/)) {
            error(true, "enroll_sout: " + data);
        } else {
            onSuccess();
        }
    });
}

//removes a badge from a scout's schedule on the server
function unenroll_scout(scout_id, badge_id, cont) {
	inactive();
    jQuery.get("/users/dropBadge/" + scout_id + "/" + badge_id + "/", null, function (data, textStatus, jqXHR) {
    	active();
        if (data.match(/\S/)) {
            error(true, "unenroll_scout: " + data);
            cont(false);
        } else {
        	cont(true);
        }
    });
}

//changes a scouts name on the server
function change_name(id, name, cont) {
    jQuery.get("/users/changeScoutName/" + id + "/" + name + "/", null, function (data, textStatus, jqXHR) {
        cont(data);
    });
}
