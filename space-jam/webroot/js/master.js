//this script is responsible for starting others
//IMPORTANT:
//In all other scripts, do not use window.onload=f; to start things when the
//page loads.  Instead, say startup_funcs.push(f);.
var startup_funcs = new Array();
window.onload = function() {
	var i;
	for (i = 0; i < startup_funcs.length; i++) {
		startup_funcs[i]();
	}
};