//Page class - basically holds everything
function Page(blocks, badges, scouts) {
    this.regTables = new Array();
    blocks.sort(function(a, b) {
		return a.getBlockTime() - b.getBlockTime();
	});
    this.blocks = blocks;
    this.badges = badges;
    this.table = $("#schedule_table");
	this.table.parent().css('overflow', 'scroll');
	this.table.parent().css('width', '850px');
    
    //put up the table header
	var row = jQuery("<tr></tr>");
	this.table.find("thead").append(row);
    row.append("<th>Name</th><th colspan=\"3\">Opts</th>");
    var i;
	for (i = this.blocks[0].getBlockTime(); i <= this.blocks[this.blocks.length - 1].getBlockTime(); i += 0.5) {
		row.append("<th>" + timeReverse(i) + "</th>");
	}
    this.types = new Array();
    for (i = 0; i < badges.length; i++) {
        var j;
        for (j = 0; j < badges[i].getTypes().length; j++) {
            if (this.types.indexOf(badges[i].getTypes()[j]) === -1) {
                this.types.push(badges[i].getTypes()[j]);
            }
        }
    }
    this.scouts = scouts;
    this.current_scout = false;
    this.current_schedule = false;
    for (i = 0; i < this.scouts.length; i++) {
    	this.current_schedule = this._newSchedule(this.scouts[i]);
    	this.current_scout = this.scouts[i];
    }
    
    //hook up the visual controls
    var button = $("#new_scout_button");
    var text = $("#new_scout_name_input");
    var select = $("#new_scout_type_input");
    var that = this;
    button.on('click', function() { that.newScout(text, select); });
    select.on('change', function() { that.typeChanged(select, button); });
    for (i = 0; i < this.types.length; i++) {
    	var opt = jQuery("<option></option>");
    	opt.attr('value', this.types[i]);
    	opt.html(this.types[i]);
    	select.append(opt);
    }
    this.typeChanged(select, button);

    //do the main setup
	var regDiv = $('#reg_table_div');
	regDiv.tabs();
	for (i = 0; i < this.types.length; i++) {
		this.regTables.push(new RegTable(
				this.types[i],
				this.blocks,
				this.badges,
				function() { return that.current_scout; },
				function() { return that.current_schedule; }));
		var div = $('#reg-div-' + this.types[i]);
		div.css('overflow', 'scroll');
		div.css('width', '855px'); //TODO would be nice to avoid hard coding this
	}
}

//the type of scout to be added was changed
Page.prototype.typeChanged = function(element_changed, element_to_update) {
	element_to_update.html("Add " + element_changed.val());
};

//creates a new scout
Page.prototype.newScout = function(name_element, type_element) {
	var that = this;
	new Scout(name_element.val(), type_element.val(), this.blocks, function(scout) { that._newScoutMade(scout); });
	name_element.val("");
};

//clones an existing scout
Page.prototype.cloneScout = function(scout) {
	var that = this;
	scout.clone(scout.getName() + "(clone)", function(scout) { that._newScoutMade(scout); });
};

//sets up the newly created scout
Page.prototype._newScoutMade = function(scout) {
	this.scouts.push(scout);
    this.current_scout = scout;
    this.current_schedule = this._newSchedule(scout);
    $('#reg_table_div').tabs('select', "#reg-div-" + scout.getType());
};

//set up a new scout's row in the table
Page.prototype._newSchedule = function(scout) {
    var that = this;
    var row = jQuery("<tr></tr>");
        
    //add the name cell
    var name = jQuery("<td></td>");
    name.html(scout.getName() + "(" + scout.typeAbriv() + ")");
    name.on('click', function() {
        that.changeName(scout, name);
    });
    row.append(name);
    
    //add the edit cell
    var edit = jQuery("<td></td>");
    edit.html("E");
    row.append(edit);

    //add the clone cell
    var clone = jQuery("<td></td>");
    clone.html("C");
    clone.on('click', function() {
        that.cloneScout(scout);
    });
    row.append(clone);

    //add the delete cell
    var del = jQuery("<td></td>");
    del.html("D");
    del.on('click', function() {
        that.deleteScout(scout, row);
    });
    row.append(del);

    //make the options clickable
    row.find("td").attr('class', 'clickable');
    
    //now build the actual schedule
	var cell = jQuery("<td></td>");
	row.append(cell);
    var ts = new TimeSection(
    		this.blocks[0].getBlockTime(),
    		this.blocks[this.blocks.length - 1].getBlockTime(),
    		scout.getType(),
    		cell,
    		function () { return scout; },
    		false);
    var i;
    for (i = 0; i < scout.getBadges().length; i++) {
    	ts.addBadge(scout.getBadges()[i]);
    }
    //now that we know the scheudle, hook up the edit button
    edit.on('click', function() {
        that.editScout(scout, row, ts);
    });
    this.table.find("tbody").append(row);
    return ts;
};

//make a scout the current scout so their schedule can be edited
Page.prototype.editScout = function(scout, element, schedule) {
	if (this.current_scout === scout) {
		return;
	}
	$("#schedule_table").append(element);
	this.current_scout = scout;
	this.current_schedule = schedule;
};

//edit a scout's name
Page.prototype.changeName = function(scout, element) {
    element.on('click', function() { });
    var text = jQuery("<input type=\"text\">" + scout.getName() + "</input>");
    element.empty();
    element.append(text);
    var that = this;
    text.on('blur', function() {
        var newName = text.val();
        element.empty();
        if (newName != "") {
            element.html(newName + "(" + scout.typeAbriv() + ")");
            element.attr('class', '');
	        scout.changeName(newName, function() {
	        	element.attr('class', 'clickable');
	            element.on('click', that.changeName(scout, element));
	            element.html(scout.getName() + "(" + scout.typeAbriv() + ")");
	        });
        } else {
            element.on('click', that.changeName(scout, element));
            element.html(scout.getName() + "(" + scout.typeAbriv() + ")");
        }
    });
    text.focus();
};

//deletes a scout
Page.prototype.deleteScout = function(scout, element) {
	var index = this.scouts.indexOf(scout);
    if (index === -1) {
        error(true, "deleteScout: scout " + scout.getId() + " not found");
        return;
    }
	this.scouts.splice(index, 1);
	scout.destroy();
    element.remove();
};

/*************************************************************************************************/

//TimeSection class - basically corresponds to a <td> with some colspan.
//Each TimeSection is part of a linked list of TimeSections that make up a <tr>.
function TimeSection(start, end, type, element, getScout, add, getSchedule) {
	this.start = start;
	this.end = end;
	this.blank = true;
	this.next = false;
	this.prev = false;
    this.type = type;
    this.element = element;
    this.getScout = getScout;
    this.add = add;
    this.getSchedule = getSchedule;
    this._newElement();
}

//returns the starting time of the TimeSection
TimeSection.prototype.getStart = function () {
	return this.start;
};

//returns the ending time of the TimeSection
TimeSection.prototype.getEnd = function() {
	return this.end;
};

//returns the next TimeSlice in this row (or flase if there isn't one)
TimeSection.prototype.getNext = function() {
    return this.next;
};

//returns the previous TimeSlice in this row (or false if there isn't one)
TimeSection.prototype.getPrev = function() {
    return this.prev;
};

//Adds a badge to this row.  Goes throught list until a suitible place is found.
//If the badge can't be fit in the row, returns false, otheriwse, returns true.
TimeSection.prototype.addBadge = function(badge) {
	var badgeStart = badge.getFirstBlock();
	var badgeEnd = badge.getLastBlock();
	if (this.blank === false) {
		if (this.next === false) {
			return false;
		}
		return this.next.addBadge(badge);
    }
    if (this.end <= badgeStart) {
        return this.next.addBadge(badge);
    }
    if (this.start > badgeStart) {
        return false;
    }
    if (this.start == badgeStart) {
        if (this.end == badgeEnd) {
            this.badge = badge;
            this.blank = false;
            this._newElement();
            return true;
        } else if (this.end > badgeEnd) {
            this._split(badgeEnd + 0.5);
            this.badge = badge;
            this.blank = false;
            this._newElement();
            return true;
        } else {
            return false;
        }
    }
    if (this.start < badgeStart) {
        if (this.end >= badgeEnd) {
            this._split(badgeStart);
            return this.next.addBadge(badge);
        } else {
            return false;
        }
    }
};

//Splits the TimeSection.  A new TimeSection (starting at time) is created and
//all blocks from the original after time are given to it.  Then the original
//is shortened and the linked list structure is updated.
TimeSection.prototype._split = function(time) {
	if (this.blank === false) {
		return false;
	}
	if (this.end - this.start == 0.5) {
		return false;
	}
	if (time <= this.start || time > this.end) {
		return false;
	}
	var newCell = jQuery("<td></td>");
	this.element.after(newCell);
	var newTS = new TimeSection(time, this.end, this.type, newCell, this.getScout, this.add, this.getSchedule);
	this.end = time - 0.5;
	this.element.attr('colspan', (this.end - this.start) * 2 + 1);
    if (this.next !== false) {
        this.next.prev = newTS;
    }
    newTS.next = this.next;
    this.next = newTS;
    newTS.prev = this;
	return newTS;
};

//drops the badge from this TimeSection and merges it with nearby ones if possible
TimeSection.prototype.removeBadge = function() {
	if (this.blank === true) {
		return false;
	}
	this.badge = false;
	this.blank = true;
	this.element.html("");
	this.element.attr('class', 'empty');
	if (this.prev !== false) {
		if (this.prev.blank === true) {
			if (this.prev.prev !== false) {
				this.prev.prev.next = this;
				this.start = this.prev.start;
				this.element.attr('colspan', (this.end - this.start) * 2 + 1);
				this.prev.element.remove();
				this.prev = this.prev.prev;
			}
		}
	}
	if (this.next !== false) {
		if (this.next.blank === true) {
			if (this.next.next !== false) {
				this.next.next.prev = this;
				this.start = this.next.start;
				this.element.attr('colspan', (this.end - this.start) * 2 + 1);
				this.next.element.remove();
				this.next = this.next.next;
			}
		}
	}
};

//updates this TimeSection's element to have proper colspan, onclick, etc.
TimeSection.prototype._newElement = function() {
	if (this.blank === false) {
        var i;
        var classes = "badge";
        for (i = 0; i < this.badge.getTypes().length; i++) {
            classes = classes + " " + this.badge.getTypes()[i];
        }
		this.element.attr('class', classes);
		if (this.add === true && this.badge.getSpots() < 50) {
			this.element.html(this.badge.getName() + " (" + this.badge.getSpots() + ")");
		} else {
			this.element.html(this.badge.getName());
		}
        var that = this;
        this.element.on('click', function() {
            if (that.add === true) {
                if (that.badge.hasCost() === true) {
                    error(false, "A $" + that.badge.getCost() + " fee will be added to your registration for each " + that.type + " that takes " + that.badge.getName() + ".");
                }
                if (that.badge.hasNote() === true) {
                    error(false, that.badge.getNote());
                }
                if (that.getScout().addBadge(that.badge) === true) {
                	that.getSchedule().addBadge(that.badge);
                	if (that.badge.getSpots() < 50) {
                		that.element.html(that.badge.getName() + " (" + that.badge.getSpots() + ")");
                	}
                }
            } else {
            	var oldClasses = that.element.attr('class');
            	that.element.attr('class', '');
                that.getScout().dropBadge(that.badge, function(worked) {
                	if (worked === true) {
                		that.removeBadge();
                	} else {
                		that.element.attr('class', oldClasses);
                	}
                });
            }
        });
	} else {
		this.element.attr('class', 'empty');
	}
	this.element.attr('colspan', (this.end - this.start) * 2 + 1);
};

/*************************************************************************************************/

//RegTable class.  Holds an entire table or badges.
//type is the type of badges (eg "Scout"), blocks are the blocks to build it
//on, and badges are the badges to look through.  Only badges of the proper
//type are used.
function RegTable(type, blocks, badges, getScout, getSchedule) {
	this.blocks = blocks;
	this.type = type;
	this.badges = badges;
    this.getScout = getScout;
    this.getSchedule = getSchedule;
	this.blocks.sort(function(a, b) {
		return a.getBlockTime() - b.getBlockTime();
	});
	this.badges.sort(function(a, b) {
		var start = a.getFirstBlock() - b.getFirstBlock();
		if (start === 0) {
			return a.getLastBlock() - b.getLastBlock();
		}
		return start;
	});
	this.firstTime = blocks[0].getBlockTime();
	this.lastTime = blocks[blocks.length - 1].getBlockTime();
	this.length = Math.round((this.lastTime - this.firstTime) * 2);
	this.table = new Array();
	
	//some inital display setup
	$("#reg_table_div").tabs("add", "#reg-div-" + this.type, this.type);
	var root = $("#reg-div-" + this.type);
	if (this.type == "Scout") {
		root.attr('class', 'scrolls');
	} else {
		root.attr('class', 'scrolls ui-tabs-hide');
	}
	var table = jQuery('<table></table>');
	root.append(table);
	table.css('border', '1');
	var row = jQuery("<tr></tr>");
	table.append(row);
    var i;
	for (i = this.firstTime; i <= this.lastTime; i += 0.5) {
		row.append("<th>" + timeReverse(i) + "</th>");
	}
    for (i = 0; i < badges.length; i++) {
        var j;
        var cont = true;
        for (j = 0; j < badges[i].getTypes().length; j++) {
            if (badges[i].getTypes()[j] === this.type) {
                cont = false;
            }
        }
        if (cont === true) {
            continue;
        }
        j = -1;
        do {
            j++;
            if (j >= this.table.length) {
            	row = jQuery("<tr></tr>");
            	table.append(row);
            	var cell = jQuery("<td></td>");
            	row.append(cell);
                this.table.push(new TimeSection(
                		this.firstTime,
                		this.lastTime,
                		this.type,
                		cell,
                		this.getScout,
                		true,
                		this.getSchedule));
            }
        } while (this.table[j].addBadge(badges[i]) === false);
    }
}

//returns this table's blocks
RegTable.prototype.getBlocks = function() {
    return this.blocks;
};

//returns this table's type
RegTable.prototype.getType = function() {
    return this.type;
};

/*************************************************************************************************/

//used to tell the user of an error
function error(critical, msg) {
    if (critical) {
        alert("This page has encountered an error.  Please contact the webmaster at <webmaster@spacejamboree.com>.  Please include what you were doing when this error apeared as well as the following message:\n" + msg);
    } else {
        alert(msg);
    }
}

//make the app "inactive" - used to show that it's loading
function inactive() {
	$('body').css('cursor', 'progress');
}

//activate the app after the loading has finished
function active() {
	$('body').css('cursor', 'auto');
}