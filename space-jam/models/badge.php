<?php
	/*********************************************************************************************
	 * badge.php                                                                                 *
	 *                                                                                           *
	 * Handles badge classes and other events.                                                   *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Jan 05, 2012                                                 Last updated Jan 05, 2012 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272.                                             *
	 *********************************************************************************************/
class Badge extends AppModel
{ 
	var $hasAndBelongsToMany = array(
		'User' => array(
			'className' => 'User',
			'joinTable' => 'badges_users',
			'foreignKey' => 'badge_id',
			'associationForeignKey' => 'user_id'
		)
	);
}