<?php
	/*********************************************************************************************
	 * media.php                                                                                 *
	 *                                                                                           *
	 * Holds the info on where media is at.                                                      *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Jul 25, 2010                                                 Last updated Feb 02, 2011 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17.                                *
	 *********************************************************************************************/
	class Media extends AppModel
	{
		public $belongsTo = 'Gallery';
		public $_schema = array
		(
			'id' => array
			(
				'type' => 'integer',
				'length' => 10
			),
			'name' => array
			(
				'type' => 'string',
				'length' => 255
			),
			'description' => array
			(
				'type' => 'text'
			),
			'created' => array
			(
				'type' => 'datetime'
			),
			'modified' => array
			(
				'type' => 'datetime'
			),
			'type' => array
			(
				'type' => 'boolean'
			),
			'code' => array
			(
				'type' => 'text'
			),
			'credit' => array
			(
				'type' => 'string',
				'length' => 255
			),
			'gallery_id' => array
			(
				'type' => 'integer',
				'length' => 11
			)
		);
		public $validate = array
		(
			'name' => array
			(
				'nameRule1' => array
				(
					'rule' => array('maxLength', 255),
					'message' => 'Name must be less than 256 characters'
				),
				'nameRule2' => array
				(
					'rule' => array('minLength', 1),
					'message' => 'Please enter a name'
				)
			),
			'id' => array
			(
				'idRule1' => array
				(
					'rule' => 'numeric',
					'message' => 'ERROR: INVALID MEDIA ID  Please contact the webmaster'
				)
			),
			'gallery_id' => array
			(
				'gallery_idRule1' => array
				(
					'rule' => 'numeric',
					'message' => 'ERROR: INVALID GALLERY ID  Please contact the webmaster'
				)
			),
			'credit' => array
			(
				'creditRule1' => array
				(
					'rule' => array('maxLength', 255),
					'message' => 'Credit must be less than 256 characters'
				)
			)/*TODO fix this to show that type must be either photo or video and video must have code
			'type' => array
			(
				'typeRule1' => array
				(
					'rule' => array('multiple' => array
					(
						'in' => array('photo', 'video'),
						'max' => 1,
						'min' => 1
					)),
					'message' => 'Please select either photo or video'
				)
			)*/
		);
	}
?>