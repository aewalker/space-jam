<?php
class Reguser extends AppModel {

	var $name = 'Reguser';
//	var $validate = array(
//		'unit_type' => array('notempty'),
//		'unit_number' => array('decimal'),
//		'council' => array('notempty'),
//		'address' => array('notempty'),
//		'city' => array('notempty'),
//		'state' => array('notempty'),
//		'zip' => array('postal'),
//		'phone1' => array('phone'),
//		'active' => array('boolean'),
//		'total_fee' => array('notempty'),
//		'amount_paid' => array('notempty'),
//		'paid' => array('boolean')
//	);
	var $hasMany = array(
		'Members' => array(
			'className' => 'User',
			'foreignKey' => 'reguser_id'
		)
	);
	var $belongsTo = array(
		'Leader' => array(
			'className' => 'User',
			'foreignKey' => 'user_id'
		)
	);
}
?>