<?php
	/*********************************************************************************************
	 * story.php                                                                                 *
	 *                                                                                           *
	 * News story model.                                                                         *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on 07/26/2010                                                   Last updated Feb 02, 2011 *
	 *                                                                                           *
	 * Copyright (c) 2010 Boy Scouts of America Crew 272 and Troop 17.                           *
	 *********************************************************************************************/
	class Story extends AppModel
	{
		public $_schema = array
		(
			'id' => array
			(
				'type' => 'integer',
				'length' => 11
			),
			'title' => array
			(
				'type' => 'string',
				'length' => 255
			),
			'contenet' => array
			(
				'type' => 'text'
			),
			'created' => array
			(
				'type' => 'datetime'
			),
			'modified' => array
			(
				'type' => 'datetime'
			),
			'author' => array
			(
				'type' => 'string',
				'length' => 255
			)
		);
		public $belongsTo = array('User' => array
		(
			'className' => 'User',
			'foreignKey' => 'author'
		));
		public $validate = array
		(
			'title' => array
			(
				'titleRule1' => array
				(
					'rule' => array('maxLength', 255),
					'message' => 'Title must be less than 256 characters'
				),
				'titleRule2' => array
				(
					'rule' => array('minLength', 1),
					'message' => 'Please enter a title'
				)
			),
			'content' => array
			(
				'contentRule1' => array
				(
					'rule' => array('minLength', 1),
					'message' => 'Please enter content'
				)
			),
			'author' => array
			(
				'authorRule1' => array
				(
					'rule' => 'isUser',
					'message' => 'ERROR: USER NOT FOUND  Please contact the webmaster'
				)
			),
			'id' => array
			(
				'idRule1' => array
				(
					'rule' => 'numeric',
					'message' => 'ERROR: INVALID STORY ID  Please contact the webmaster'
				)
			)
		);
		
		protected function isUser($check)
		{
			$value = array_values($check);
			$value = $value[0];
			
			$result = $this->User->find('first', array('conditions' => array('username' => $value)));
			if ($result == array())
			{
				return false;
			}
			else
			{
				return true;
			}
		}
	}
?>