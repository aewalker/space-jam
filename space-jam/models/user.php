<?php
	/*********************************************************************************************
	 * user.php                                                                                  *
	 *                                                                                           *
	 * Handles users' logins and sessions.                                                       *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Jul 26, 2010                                                 Last updated Nov 02, 2011 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17 except where noted.             *
	 *********************************************************************************************/
class User extends AppModel
{
	public $displayField = 'username';
	var $actsAs = 'ExtendAssociations'; 
	var $belongsTo = 'Reguser';
	var $hasAndBelongsToMany = array(
		'Badge' => array(
			'className' => 'Badge',
			'joinTable' => 'badges_users',
			'foreignKey' => 'user_id',
			'associationForeignKey' => 'badge_id'
		)
	);
}
//	class User extends AppModel
//	{
//		public $displayField = 'username';
//		public $primaryKey = 'username';
//		public $_schema = array
//		(
//			'username' => array
//			(
//				'type' => 'string'
//			),
//			'password' => array
//			(
//				'type' => 'string',
//				'length' => 40
//			),
//			'created' => array
//			(
//				'type' => 'datetime'
//			),
//			'modified' => array
//			(
//				'type' => 'datetime'
//			),
//			'admin' => array
//			(
//				'type' => 'boolean'
//			)
//		);
//		public $validate = array
//		(
//			'username' => array
//			(
//				'usernameRule1' => array
//				(
//					'rule' => 'alphaNumeric',
//					'message' => 'Username must contain only numbers and letters'
//				),
//				'usernameRule2' => array
//				(
//					'rule' => array('between', 4, 20),
//					'message' => 'Username must be between 4 and 20 characters long'
//				),
//				'usernameRule3' => array
//				(
//					'rule' => '_isUnique',
//					'message' => 'That username is already taken; please choose another'
//				)
//			),
//			'password' => array
//			(
//				'passwordRule1' => array
//				(
//					'rule' => array('minLength', 6),
//					'message' => 'Password must be at least 6 characters long'
//				),
//				'passwordRule2' => array
//				(
//					'rule' => 'hasAllTypes',
//					'message' => 'Password must contain at least one letter and one number'
//				),
//				'passwordRule3' => array
//				(
//					'rule' => array('identicalFieldValues', 'confirm'),
//					'message' => 'Passwords did not match; try again'
//				)
//			)
//		);
//
//		protected function hasAllTypes($check)
//		{
//			$value = array_values($check);
//			$value = $value[0];
//			
//			if (preg_match('/[[:digit:]]/', $value) == false)
//				return false;
//			if (preg_match('/[[:alpha:]]/i', $value) == false)
//				return false;
//				
//			return true;
//		}
//		
//		protected function identicalFieldValues($field = array(), $compare_field = null) 
//	    { //This function's code by Aran Johnson
//	      //from http://bakery.cakephp.org/articles/view/using-equalto-validation-to-compare-two-form-fields
//    	    foreach ($field as $key => $value)
//    	    {
//	            $v1 = $value;
//	            $v2 = $this->data[$this->name][$compare_field];                 
//	            if ($v1 !== $v2)
//	            {
//	                return false;
//	            }
//	        }
//        return true;
//    	}
//    	
//    	function _isUnique($check)
//    	{
//    		foreach ($check as $field => $value)
//    		{
//    			if ($this->find('first', array('conditions' => array($field => $value))) != array())
//    			{
//    				return false;
//    			}
//    		}
//    		return true;
//        } 
//    	
//    	function beforeSave()
//    	{
//    		$this->data['User']['password'] = sha1($this->data['User']['password']);
//    		unset($this->data['User']['confirm']);
//    		return parent::beforeSave();
//    	}
//	}
?>