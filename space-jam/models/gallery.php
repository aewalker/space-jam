<?php
	/*********************************************************************************************
	 * gallery.php                                                                               *
	 *                                                                                           *
	 * Media gallery model.                                                                      *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Jul 26, 2010                                                 Last updated Feb 03, 2011 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17.                                *
	 *********************************************************************************************/
	class Gallery extends AppModel
	{
		public $_schema = array
		(
			'id' => array
			(
				'type' => 'integer',
				'length' => 11
			),
			'name' => array
			(
				'type' => 'string',
				'length' => 255
			),
			'description' => array
			(
				'type' => 'text'
			),
			'created' => array
			(
				'type' => 'datetime'
			),
			'modified' => array
			(
				'type' => 'datetime'
			)
		);
		public $validate = array
		(
			'name' => array
			(
				'nameRule1' => array
				(
					'rule' => array('maxLength', 255),
					'message' => 'Name must be less than 256 characters'
				),
				'nameRule2' => array
				(
					'rule' => array('minLength', 1),
					'message' => 'Please enter a name'
				)
			),
			'id' => array
			(
				'idRule1' => array
				(
					'rule' => 'numeric',
					'message' => 'ERROR: INVALID GALLERY ID  Please contact the webmaster'
				)
			)
		);
	}
?>