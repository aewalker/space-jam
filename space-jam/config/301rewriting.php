<?php
/*---------------------------------------------------------------------------*
 * 301rewriting.php                                                          *
 *                                                                           *
 * Redirects things like spacejamboree.com/index.html to spacejambore.com/   *
 * with a Google-friendly 301 code.                                          *
 *---------------------------------------------------------------------------*/
$url = $_SERVER['REQUEST_URI'];
if (check301($url))
{
	header('HTTP/1.1 301 Moved Permanently');
	header("Location: $url");
	exit();
}

//check for all the common things
function check301(&$url)
{
	//common URL suffixes to check for
	$suffixes = array
	(
		'index.php',
		'index.htm',
		'index.html',
		'index.shtml',
		'index.asp',
		'default.asp',
		'index.aspx',
		'index.cfm',
		'index.pl',
		'default.htm',
		'index.py',
		'index.jsp',
		'index.psp'
	);
	//the old pages (from the original spacejamboree.com) to redirect
	$oldPages = array
	(
		array('old' => 'faq.php', 'new' => 'pages/about'),
		array('old' => 'multimedia.php', 'new' => 'media/'),
		array('old' => 'patches.php', 'new' => 'media/view/2'),
		array('old' => 'about.php', 'new' => 'pages/about'),
		array('old' => 'contact.php', 'new' => 'pages/contact')
	);
	$returnVal = false;
	
	foreach ($suffixes as $suffix)
	{
		if (substr($url, strlen($suffix) * -1) == $suffix)
		{
			$url = substr($url, 0, strlen($suffix) * -1);
			$returnVal = true;
		}
	}
	foreach ($oldPages as $oldPage)
	{
		if (substr($url, strlen($oldPage['old']) * -1) == $oldPage['old'])
		{
			$url = substr($url, 0, strlen($oldPage['old']) * -1).$oldPage['new'];
			$returnVal = true;
		}
	}
	return $returnVal;
}
?>
