<?php
	/*********************************************************************************************
	 * media/create.ctp                                                                          *
	 *                                                                                           *
	 * Option for admins to create a new media gallery.                                          *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Jul 27, 2010                                                 Last updated Nov 22, 2010 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17.                                *
	 *********************************************************************************************/
$this->set("title_for_layout", array('title' => 'Create Gallery'));
?><div id="content">
	<?php if ($this->Session->check('User.isAdmin'))
		{
			if (!isset($result))
			{ //No data was entered, so show the form
				echo $form->create('Gallery', array('url' => '/media/create/submit')).
				'<p>Gallery Name</p>'.
				$form->text('Gallery.name').
				'<p>Gallery Description</p>'.
				$form->textarea('Gallery.description', array(
				'cols' => 70)).
				'<br/><br/>'.
				$form->submit('Create').
				$form->end();
			}
			else
			{ //data was just submitted, so show result
				if ($result === true)
					echo '<h3>Success!  Gallery created.</h3>';
				else
				{
					echo '<h3>Failure!  '.count($result).' error(s):</h3>';
					echo '<div id="error">';
					foreach ($result as $field => $message)
						echo '<p>'.ucwords($field).': '.$message.'</p>';
					echo '</div>';
					echo $html->link('Try Again', '/media/create/');
				}
			}
		}
		else
			echo '<h3>Only admins may create galleries.</h3>';
		echo '<h3>'.$html->link('Back', '/media/').'<h3>'; ?>
</div>
<div id="sidebar">
	<h1>Create a new multimedia gallery.</h1>
</div>
