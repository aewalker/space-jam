<?php
	/*********************************************************************************************
	 * media/view.ctp                                                                            *
	 *                                                                                           *
	 * Shows a particular gallery.                                                               *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Jul 26, 2010                                                 Last updated Dec 05, 2010 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17.                                *
	 *********************************************************************************************/
if ($gallery['Gallery']['id'] == PATCHES_GALLERY_ID)
	$this->set('currentNavButton', 'Patches');
else
	$this->set('currentNavButton', 'Multimedia');
?><div id="content">
	<?php echo '<h1>'.$gallery['Gallery']['name'].'</h1>';
	$this->set("title_for_layout",
		array('title' => 'View Gallery', 'description' => $gallery['Gallery']['name']));
	if ($this->Session->check('User.isAdmin'))
	{
		echo '<h3>'.$html->link('Edit', '/media/edit/'.$gallery['Gallery']['id']).' ';
		echo $html->link('Add Content', '/media/add/'.$gallery['Gallery']['id']).' ';
		echo $html->link('Delete', '/media/delete/'.$gallery['Gallery']['id']).'</h3>';
	}
	echo '<p>'.$gallery['Gallery']['description'].'</p>';
	foreach ($data as $item)
	{
		echo '<h2>'.$item['Media']['name'].'</h2>';
		if ($this->Session->check('User.isAdmin'))
		{
			echo '<h3>'.$html->link('Remove', '/media/remove/'.$item['Media']['id']).' ';
			echo $html->link('Edit', '/media/alter/'.$item['Media']['id']).'</h3>';
		}
		echo '<p>'.$item['Media']['description'].'</p>';
		if ($item['Media']['type'] == 'video')
			echo $item['Media']['code'];
		if ($item['Media']['type'] == 'photo')
		{
			echo $html->image('galleries/'.$gallery['Gallery']['id'].
			'/'.$item['Media']['id']);
		}
		if (!empty($item['Media']['credit']))
			echo '<p>Credit: '.$item['Media']['credit'].'</p>';
	}
	echo '<p>';
	if ($this->Paginator->hasPrev())
		echo $this->Paginator->prev('« Previous', null, null, array('class' => 'disabled')).' ';
	echo $this->Paginator->numbers();
	if ($this->Paginator->hasNext())
		echo ' '.$this->Paginator->next('Next »', null, null, array('class' => 'disabled')); 
	echo '</p><p>Page '.$this->Paginator->counter().'</p>';
	echo '<h3>'.$html->link('Back', '/media/').'</h3>'; ?>
</div>
<div id="sidebar">
	<h1>View Media Gallery</h1>
</div>
