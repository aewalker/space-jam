<?php
	/*********************************************************************************************
	 * media/missing.ctp                                                                         *
	 *                                                                                           *
	 * Displays a message to say that the selected gallery doesn't exist.                        *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Jul 26, 2010                                                 Last updated Dec 05, 2010 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17.                                *
	 *********************************************************************************************/
$this->set("title_for_layout", array('title' => 'Missing Gallery'));
$this->set('currentNavButton', 'Multimedia');
?><div id="content">
	<h3>Missing Gallery</h3>
	<p>The selected gallery does not exist or has no content. 
	If you reached this page from a link on the Space Jamboree website, please <?php
	echo $html->link('email the webmaster', 'mailto:awalker@spacejamboree.com'); ?>.</p>
	<?php echo '<h3>'.$html->link('Back', '/media/').'</h3>'; ?>
</div>
<div id="sidebar">
	<?php if ($this->Session->check('User.isAdmin'))
	{
		echo '<h3>'.$html->link('Create Gallery', '/media/create/').'</h3>';
	} ?>
	<h1>Galleries</h1>
	<?php 
	for ($i = 0; $i < $count; $i++)
	{
		echo '<p>'.($i+1).'. '.$html->link($galleries[$i]['Gallery']['name'],
		'/media/view/'.urlencode($galleries[$i]['Gallery']['id']), array(
		'title' => strip_tags($galleries[$i]['Gallery']['description']))).'</p>';
	} ?>
</div>
