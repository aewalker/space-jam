<?php
	/*********************************************************************************************
	 * media/remove.ctp                                                                          *
	 *                                                                                           *
	 * Option for admins to remove content from a gallery.                                       *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Jul 27, 2010                                                 Last updated Nov 22, 2010 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17.                                *
	 *********************************************************************************************/
$this->set("title_for_layout", array('title' => 'Remove Media'));
?><div id="content">
	<?php if ($this->Session->check('User.isAdmin'))
		{
			if (!isset($result))
			{
				echo '<h2>Are you sure you want to delete '.$media['Media']['name'].'?</h2>';
				echo '<h3>This action cannot be undone.</h3>';
				
				echo $form->create('Media', array('url' => '/media/remove/'.$media['Media']['id'].'/true'));
				echo $form->submit('Yes, I\'m sure');
				echo $form->end();
				
				echo $form->create('Media', array('url' => '/media/'));
				echo $form->submit('No, go back');
				echo $form->end();
			}
			else
			{
				if ($result === true)
					echo '<h3>Success!  Media deleted.</h3>';
				else
					echo '<h3>Failure!  Could not delete media.</h3>'.
					'<p>Please '.$html->link('email the webmaster', 'mailto:webmaster@spacejamboree.com').
					' and inlcude the text of this page '.
					'(press Ctrl-a then Ctrl-c to copy the text on this page).';
				echo '<h3>'.$html->link('Back', '/media/').'<h3>';
			}
		}
		else
		{
			echo '<h3>Only admins may delete galleries.</h3>';
			echo '<h3>'.$html->link('Back', '/media/').'<h3>';
		} ?>
</div>
<div id="sidebar">
	<h1>Remove content from a multimedia gallery.</h1>
</div>
