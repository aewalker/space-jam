<?php
	/*********************************************************************************************
	 * media/edit.ctp                                                                            *
	 *                                                                                           *
	 * Option for admins to edit an gallery: add, remove, or edit photos, etc.                   *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Jul 27, 2010                                                 Last updated Nov 22, 2010 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17.                                *
	 *********************************************************************************************/
$this->set("title_for_layout", array('title' => 'Edit Gallery'));
?><div id="content">
	<?php if ($this->Session->check('User.isAdmin'))
		{
			if (!isset($result))
			{ //no data submited, so show the form
				echo $form->create('Gallery',
					array('url' => '/media/edit/'.$gallery['Gallery']['id'].'/submit')).
				'<p>Gallery Name</p>'.
				$form->text('Gallery.name', array('value' => $gallery['Gallery']['id'])).
				'<p>Gallery Description</p>'.
				$form->textarea('Gallery.description',
					array('cols' => 70, 'value' => $gallery['Gallery']['description'])).
				'<br/><br/>'.
				$form->submit('Edit').
				$form->end();
			}
			else
			{ //data was just submitted, so show result
				if ($result === true)
					echo '<h3>Success!  Gallery edited.</h3>';
				else
				{
					echo '<h3>Failure!  '.count($result).'error(s):</h3>';
					echo '<div id="error">';
					foreach ($result as $field => $message)
						echo '<p>'.ucwords($field).': '.$message.'</p>';
					echo '</div>';
					echo $html->link('Try Again', '/media/edit/'.$id);
				}
			}
		}
		else
			echo '<h3>Only admins may edit galleries.</h3>';
		echo '<h3>'.$html->link('Back', '/media/').'<h3>'; ?>
</div>
<div id="sidebar">
	<h1>Edit a multimedia gallery.</h1>
</div>
