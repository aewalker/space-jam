<?php
	/*********************************************************************************************
	 * media/add.ctp                                                                             *
	 *                                                                                           *
	 * Allows admins to add to existing galleries.                                               *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Jul 27, 2010                                                 Last updated Nov 22, 2010 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17.                                *
	 *********************************************************************************************/
$this->set("title_for_layout", array('title' => 'Add Media'));
?><div id="content">
	<?php if ($this->Session->check('User.isAdmin'))
		{
			if (!isset($type) && !isset($result))
			{
				echo '<h1>Add to '.$gallery['Gallery']['name'].'.</h1>';
				echo '<h3>Select type of content to add:</h3>';
				
				echo $form->create('Media', array('url' => '/media/add/'.$gallery['Gallery']['id'].'/video')).
				$form->submit('Video').
				$form->end();
				
				echo $form->create('Media', array('url' => '/media/add/'.$gallery['Gallery']['id'].'/photo')).
				$form->submit('Photo').
				$form->end();
			}
			else if (isset($type))
			{
				if ($type == 'video')
				{
					echo '<h1>Add a video to '.$gallery['Gallery']['name'].'.</h1>';
					echo $form->create('Media', array('url' => '/media/add/'.$gallery['Gallery']['id'].'/submit')).
					'<p>Name</p>'.$form->text('Media.name').
					'<p>Description</p>'.$form->textarea('Media.description', array('cols' => 70)).
					'<p>Embed Code</p>'.$form->text('Media.code').
					'<p>Credit</p>'.$form->text('Media.credit').
					$form->hidden('Media.gallery_id', array('value' => $gallery['Gallery']['id'])).
					$form->hidden('Media.type', array('value' => 'video')).'<br/><br/>'.
					$form->submit('Add').$form->end();
				}
				else
				{
					echo '<h1>Add a photo to '.$gallery['Gallery']['name'].'.</h1>';
					echo $form->create('Media', array('url' => '/media/add/'.$gallery['Gallery']['id'].'/submit',
						'enctype' => 'multipart/form-data')).
					'<p>Name</p>'.$form->text('Media.name').
					'<p>Description</p>'.$form->textarea('Media.description', array('cols' => 70)).
					'<p>File</p>'.$form->file('file').
					'<p>Credit</p>'.$form->text('Media.credit').
					$form->hidden('Media.gallery_id', array('value' => $gallery['Gallery']['id'])).
					$form->hidden('Media.type', array('value' => 'photo')).'<br/><br/>'.
					$form->submit('Add').$form->end();
				}
			}
			else
			{
				if ($result === true)
					echo '<h3>Success!  Content added.</h3>';
				else
				{
					echo '<h3>Failure!  '.count($result).'error(s):</h3>';
					echo '<div id="error">';
					foreach ($result as $field => $message)
						echo '<p>'.ucwords($field).': '.$message.'</p>';
					echo '</div>';
					echo $html->link('Try Again', '/media/add/'.$id);
				}
			}
		}
		else
			echo '<h3>Only admins may add to galleries.</h3>';
		echo '<h3>'.$html->link('Back', '/media/').'<h3>'; ?>
</div>
<div id="sidebar">
	<h1>Add to an existing multimedia gallery.</h1>
</div>
