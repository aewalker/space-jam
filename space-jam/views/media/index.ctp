<?php
	/*********************************************************************************************
	 * media/index.ctp                                                                           *
	 *                                                                                           *
	 * Shows an overview of multimedia on the site.                                              *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Jul 25, 2010                                                 Last updated Jun 14, 2011 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17.                                *
	 *********************************************************************************************/
$this->set("title_for_layout", array('title' => 'Media',
	'description' => 'View multimedia from the Space Jamboree Merit Badge Workshop'));
$this->set('currentNavButton', 'Media');
?><div id="content">
	<?php if (isset($latest))
	{ ?>
		<h1>Multimedia</h1>
		<p>Check out the different videos and photo albums to get a tase of Space Jamboree. 
		Here's our latest <?php echo $latest['Media']['type']; ?>:</p>
		<?php echo '<h2>'.$latest['Media']['name'].'</h2>';
		echo '<p>'.$latest['Media']['description'].'</p>';
		if ($latest['Media']['type'] == 'video')
			echo $latest['Media']['code'];
		if ($latest['Media']['type'] == 'photo')
			echo $html->image('galleries/'.$latest['Media']['gallery_id'].
			'/'.$latest['Media']['id']);
		if (!empty($latest['Media']['credit']))
			echo '<p>Credit: '.$latest['Media']['credit'].'</p>';
		echo '<p>From '.
		$html->link($latest['Gallery']['name'], '/media/view/'.$latest['Gallery']['id']).
		'<p>';
	}
	else
	{ //in this case, the menu has been requested, so show that in the main area
		foreach ($data as $gallery)
			echo '<p>'.$html->link($gallery['Gallery']['name'],
			'/media/view/'.$gallery['Gallery']['id'], array(
			'title' => strip_tags($gallery['Gallery']['description']))).'</p>';
		echo '<p>';
		if ($this->Paginator->hasPrev())
			echo $this->Paginator->prev('« Previous', null, null, array('class' => 'disabled')).' ';
		echo $this->Paginator->numbers();
		if ($this->Paginator->hasNext())
			echo ' '.$this->Paginator->next('Next »', null, null, array('class' => 'disabled')); 
		echo '</p><p>Page '.$this->Paginator->counter().'</p>';
	}
	echo '<h3>'.$html->link('Back to Home Page', '/').'</h3>'; ?>
	
</div>
<div id="sidebar">
	<?php if ($this->Session->check('User.isAdmin'))
		echo '<h3>'.$html->link('Create Gallery', '/media/create/').'</h3>';
	if (isset($latest))
	{
		echo '<h1>Galleries</h1>';
		foreach ($data as $gallery)
			echo '<p>'.$html->link($gallery['Gallery']['name'],
			'/media/view/'.$gallery['Gallery']['id'], array(
			'title' => strip_tags($gallery['Gallery']['description']))).'</p>';
		echo '<strong>'.$html->link('See all galleries', '/media/index/showList').'</strong>';
	}
	else
	{
		?><h1>Multimedia Galleries</h1><?php
	} ?>
</div>
