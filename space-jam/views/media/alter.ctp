<?php
	/*********************************************************************************************
	 * media/alter.ctp                                                                           *
	 *                                                                                           *
	 * Allows an admin to change the content of a particular item.                               *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Jul 30, 2010                                                 Last updated Nov 22, 2010 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17.                                *
	 *********************************************************************************************/
$this->set("title_for_layout", array('title' => 'Edit Media'));
?><div id="content">
	<?php if ($this->Session->check('User.isAdmin'))
		{
			if (!isset($result))
			{ //user hasn't submitted the form
				echo '<h1>Edit '.$media['Media']['name'].'</h1>';
				echo $form->create('Media', array('url' => '/media/alter/'.$media['Media']['id'].'/submit')).
				'<p>Name</p>'.
				$form->text('Media.name', array('value' => $media['Media']['name'])).
				'<p>Description</p>'.
				$form->textarea('Media.description', array('value' => $media['Media']['description']));
				if ($media['Media']['type'] == 'video')
					echo '<p>Embed Code</p>'.$form->text('Media.code', array('value' => $media['Media']['code']));
				echo '<p>Credit</p>'.
				$form->text('Media.credit', array('value' => $media['Media']['credit'])).
				$form->submit('Edit').
				$form->end();
			}
			else
			{
				if ($result === true)
					echo '<h3>Success!  Item edited.</h3>';
				else
				{
					echo '<h3>Failure! '.count($result).' error(s)</h3>';
					echo '<div id="error">';
					foreach ($result as $field => $message)
						echo '<p>'.ucwords($field).': '.$message.'</p>';
					echo '</div>';
					echo $html->link('Try Again', '/media/alter/'.$id);
					$html->link('Back', '/media/');
				}
			}
		}
		else
			echo '<h3>Only admins may edit multimedia items.</h3>';
		echo '<h3>'.$html->link('Back', '/media/').'<h3>'; ?>
</div>
<div id="sidebar">
	<h1>Edit a multimedia item.</h1>
</div>
