<?php
/*************************************************************************************************
 * default.ctp                                                                                   *
 *                                                                                               *
 * Default layout for the Space Jamboree site.  Has a header with an image, a nav bar and then   *
 * the content followed by a footer bar.                                                         *
 *                                                                                               *
 * Written by Austin Walker                                              awalkerenator@gmail.com *
 * on 07/25/2010                                                    Last updated on Jun 14, 2011 *
 *                                                                                               *
 * Copyright (c) 2010 Boy Scouts Of America Crew 272 and Troop 17.                               *
 *************************************************************************************************/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php
			if (!isset($title_for_layout['description']))
				$title_for_layout['description'] = 'The midwest\'s largest high-tech Merit Badge workshop for Boy Scouts and Girl Scouts';
			echo 'Space Jamboree | ';
			echo $title_for_layout['title'];
			echo ' | ';
			echo $title_for_layout['description'];
		?>
	</title>
	<script src="/js/master.js" type="text/javascript"></script>
	<?php
		echo $this->Html->css('space.jam');
		echo $html->meta('space.jam.ico', '/img/space.jam.ico', array('type' => 'icon'));
		echo $html->meta('description', $title_for_layout['description']);
		echo $scripts_for_layout;
		echo $html->script('twinkle');
	?>
</head>

<body>
	<div id="wrap">
		<div id="header">
			<?php echo $html->link( 
			$html->image('space.jam.banner.png'),
			'/', array('escape' => false)); ?>
			
		</div>
		
		<div id="navbar">
		<table class="borderless" id="navBarTable">
			<tr class="borderless">
				<td class="borderless" class="square"></td>
				<td class="borderless" rowspan="3">
			<?php
			//Now to build the main navigation links at the top:
			//We want to put each link as an h1 for Google.
			//We also want to highlight the current page for the user
			
			//create an array of the 7 areas
			$navButtons = array(
//			array(
//				'name' => 'Home',
//				'url' => '/',
//				'description' => 'Space Jamboree Home'
//			),
			array(
				'name' => 'Register',
				'url' => '/pages/registration/',
				'description' => 'Download Registration Forms'
			),
			array(
				'name' => 'News',
				'url' => '/stories/',
				'description' => 'Read About the Latest News'
			),
			array(
				'name' => 'Media',
				'url' => '/media/',
				'description' => 'View Photos and Videos'
			),
			array(
				'name' => 'Patches',
				'url' => '/media/view/2',
				'description' => 'Past Space Jamboree Patches'
			),
			array(
				'name' => 'About',
				'url' => '/pages/about/',
				'description' => 'About Space Jamboree'
			),
			array(
				'name' => 'Contacts',
				'url' =>'/pages/contacts/',
				'description' => 'Contact Space Jamboree'
			),
			array(
				'name' => 'Fees/Sponsors',
				'url' => '/pages/sponsors/',
				'description' => 'Check Out Our Spnsors'
			));
			
			//Now, since the view tells us where we are, we can just assign a special id
			//to the navigation button for the current location
			echo '<table class="borderless" id="navButtonTable"><tr id="top">';
			foreach ($navButtons as $navButton)
			{ //Set up the top row of the table
				if (isset($currentNavButton) && $navButton['name'] == $currentNavButton)
				{
					echo '<td class="borderless square"></td>';
					echo '<td class="borderless square" id="topLeftCurrent"></td>';
					echo '<td class="borderless long current"></td>';
					echo '<td class="borderless square" id="topRightCurrent"></td>';
					echo '<td class="borderless square"></td>';
				}
				else
				{
					echo '<td class="borderless square"></td>';
					echo '<td class="borderless long"></td>';
					echo '<td class="borderless square"></td>';
				}
			}
			echo '</tr><tr id="mid">';
			foreach ($navButtons as $navButton)
			{ //middle row, where the main links are
				if (isset($currentNavButton) && $navButton['name'] == $currentNavButton)
				{
					echo '<td class="borderless tall"></td>';
					echo '<td class="borderless tall current"></td>';
					echo '<td class="borderless current"><h1 class="navButton">';
					echo $html->link($navButton['name'], $navButton['url'], array('title' => $navButton['description']));
					echo '</h1></td>';
					echo '<td class="borderless tall current"></td>';
					echo '<td class="borderless tall"></td>';
				}
				else
				{
					echo '<td class="borderless tall"></td>';
					echo '<td><h1 class="borderless navButton">';
					echo $html->link($navButton['name'], $navButton['url'], array('title' => $navButton['description']));
					echo '</h1></td>';
					echo '<td class="borderless tall"></td>';
				}
			}
			echo '</tr><tr class="borderless"  id="bottom">';
			foreach ($navButtons as $navButton)
			{ //bottom row
			if (isset($currentNavButton) && $navButton['name'] == $currentNavButton)
				{
					echo '<td class="borderless square" id="bottomLeftCurrent"></td>';
					echo '<td class="borderless square current"></td>';
					echo '<td class="borderless long current"></td>';
					echo '<td class="borderless square current"></td>';
					echo '<td class="borderless square" id="bottomRightCurrent"></td>';
				}
				else
				{
					echo '<td class="borderless square"></td>';
					echo '<td class="borderless long"></td>';
					echo '<td class="borderless square"></td>';
				}
			}
			echo '</tr></table>';
			 ?>
			 	</td>
			 	<td class="borderless square"></td>
			 </tr>
			 <tr>
			 	<td class="borderless tall"></td>
			 	<td class="borderless tall"></td>
			 </tr>
			 <tr>
			 	<td class="borderless square" id="navBarBottomLeft"></td>
			 	<td class="borderless square" id="navBarBottomRight"></td>
			 </tr>
		</table>
		</div>
		<?php if ($this->Session->check('Auth.User'))
			{
				echo '<div id="loginbar">';
			/*{ 
				?><div id="login"><?php
				echo $form->create('User', array('url' => '/users/login/', 'div' => false)).
				'Username: '.
				$form->text('User.username', array('div' => false)).' Password: '.
				$form->password('User.password', array('div' => false)).' '.
				$form->submit('Login', array('div' => false)).
				' Not a member? '.$html->link('Sign up', '/users/register/').
				$form->end();
				?></div><?php
			}
			else*/
				?><div id="loggedin"><?php
				echo 'Logged in as '.$this->Session->read('Auth.User.username').
				'.  '.$html->link('Logout', '/users/logout/').'.';
				?></div><?php
			if ($this->Session->read('Auth.User.typeofuser') == 'admin')
			{ ?>
			<div id="admin">&spades;&hearts;&clubs;&diams;ADMIN MODE&diams;&clubs;&hearts;&spades;</div>
			<?php
			}
			echo '</div>';
			} ?>
		
		<div id="main"> 
			<div id="spacer"></div>
			<?php echo $content_for_layout; ?>
			<div id="clear"></div>
			<div id="column">
			<?php
				$colors = array('#660000', '#990000', '#AA0000', '#BB0000', '#BB0000', '#FF0000', 							'#BB0000', '#AA0000', '#990000', '#660000');				
				for ($i = 0; $i < 10; $i++)
				{
					echo "<div class=\"border\" style=\"border-right: 2px solid".$colors[$i].
						"; border-left: 2px solid ".$colors[$i]."; width: ".
						(900 - 4 * ($i + 1))."px;\">";
				}
				for ($i = 0; $i < 10; $i++)
				{
					echo "</div>";
				}
			?>
			</div>
		</div>
		
		<div id="loginbar">
			<?php if ($this->Session->read('Auth.User.typeofuser' == 'admin'))
			{ ?>
			<div id="admin">&spades;&hearts;&clubs;&diams;ADMIN MODE&diams;&clubs;&hearts;&spades;</div>
			<?php } 
			if ($this->Session->check('Auth.User'))
			/*{ 
				?><div id="login"><?php
				echo $form->create('User', array('url' => '/users/login/', 'div' => false)).
				'Username: '.
				$form->text('User.username', array('div' => false)).' Password: '.
				$form->password('User.password', array('div' => false)).' '.
				$form->submit('Login', array('div' => false)).
				' Not a member? '.$html->link('Sign up', '/users/register/').
				$form->end();
				?></div><?php
			}
			else*/
			{
				?><div id="loggedin"><?php
				echo 'Logged in as '.$this->Session->read('Auth.User.username').
				'.  '.$html->link('Logout', '/users/logout/').'.';
				?></div><?php
			} ?>
		</div>
		
		<div id="footer">
			<?php $plugs = array(
			'images' => array('bsa', 'eclipse', 'php', 'cake', 'linux', 'ubuntu', 'venturing',
			'gsa'),
			'sites' => array('scouting.org', 'eclipse.org', 'php.net', 'cakephp.org', 'linux.org',
			'ubuntu.com', 'scouting.org', 'girlscouts.org'),
			'names' => array('Boy Scouts of America', 'Eclipse', 'PHP', 'CakePHP', 'Linux', 'Ubuntu', 				'Venturing BSA', 'Girl Scouts of America'),
			'descriptions' => array('Scouting', 'Eclipse.org home', 'PHP: Hypertext Preprocessor', 'CakePHP: the rapid development php framework', 'The Linux Home Page at Linux Online', 'Ubuntu homepage | Ubuntu', 'Scouting', 'Girl Scouts of the USA: Official Web Site')
			);
			$count = count($plugs['sites']);
			
			for ($i = 0; $i < $count; $i++)
			{
				echo $html->link(
					$html->image('./plugs/'.$plugs['images'][$i].'.logo.png', array('alt' => $plugs['names'][$i])),
					'http://www.'.$plugs['sites'][$i].'/',
					array('escape' => false, 'target' => '_blank', 'title' => $plugs['descriptions'][$i])
				);
			} ?>
			<p id="copyright">Copyright &copy; 2009-2011 Boy Scouts of America Crew 272 and Troop 17. <?php echo $html->link('Admin Login', '/users/login/')?></p>
		</div>
	</div>
</body>
</html>
