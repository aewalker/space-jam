<?php
	/*********************************************************************************************
	 * stories/delete.ctp                                                                        *
	 *                                                                                           *
	 * Displays the success or failure upon deletion of a story.                                 *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Jul 25, 2010                                                 Last updated Jul 25, 2010 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17.                                *
	 *********************************************************************************************/
$this->set("title_for_layout", array('title' => 'Delete Story'));
?><div id="content">
	<?php if ($this->Session->check('User.isAdmin'))
	{
		if (!isset($result))
		{
			echo '<h2>Are you sure you want to delete this story?</h2>';
			echo '<h3>This action cannot be undone.</h3>';
			echo $form->create('Story', array('url' => '/stories/delete/'.$id));
			echo $form->hidden('confirm', array('value' => 'true'));
			echo $form->submit('Yes');
			echo $form->end();
			echo $form->create('Story', array('url' => '/stories/'));
			echo $form->submit('No');
			echo $form->end();
		}
		else
		{
			if ($result == true)
			{
				echo '<h3>Success!  Story delted.</h3>';
			}
			else
			{
				echo '<h3>Failure!  Could not delete story.</h3>';
			}
			echo '<h3>'.$html->link('Back', '/stories/').'<h3>';
		}
	}
	else
	{
		echo '<h3>Only admins may delete stories.</h3>';
		echo '<h3>'.$html->link('Back', '/stories/').'<h3>';
	} ?>
</div>
