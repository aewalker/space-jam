<?php
	/*********************************************************************************************
	 * stories/index.ctp                                                                         *
	 *                                                                                           *
	 * Shows all the current news stories.                                                       *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Jul 25, 2010                                                 Last updated Jun 15, 2011 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17.                                *
	 *********************************************************************************************/
$this->set("title_for_layout", array('title' => 'News', 'description' => 'The lastest stories about the Space Jamboree Merit Badge Workshop'));
$this->set('currentNavButton', 'News');
?><div id="content">
	<h1>Latest News</h1>
	<?php
	foreach ($stories as $story)
	{
		echo '<h2>'.$html->link($story['title'], '/stories/view/'.$story['id']).'</h2>';
		echo '<p>'.strip_tags($story['content']).'<br/>
		<strong>'.$html->link('Read More', '/stories/view/'.$story['id']);
		if ($this->Session->check('User.isAdmin'))
		{
			echo ' '.$html->link('Edit', '/stories/edit/'.$story['id']).' '.
			$html->link('Delete', '/stories/delete/'.$story['id']);
		}
		echo '</strong></p>';
	} ?>
	<h3><?php echo $html->link('Back to Home Page', '/'); ?></h3>
</div>
<div id="sidebar">
	<?php if ($this->Session->check('User.isAdmin'))
		{
			echo '<h3>'.$html->link('Create Story', '/stories/create/');
		} ?>
</div>
