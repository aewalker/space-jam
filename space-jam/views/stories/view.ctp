<?php
	/*********************************************************************************************
	 * stories/view.ctp                                                                          *
	 *                                                                                           *
	 * Shows a particular news story.                                                            *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on 07/26/2010                                                     Last updated 01/05/2012 *
	 *                                                                                           *
	 * Copyright (c) 2010 Boy Scouts of America Crew 272 and Troop 17.                           *
	 *********************************************************************************************/
	$this->set('currentNavButton', 'News');
?><div id="content">
	<?php
	echo '<h1>'.$story['Story']['title'].'</h1>';
	$this->set('title_for_layout', array('title' => 'View Story', 'description' => $story['Story']['title']));
	echo '<p><em>'.$story['Story']['created'].' - '.$author['User']['name'].' '.
	', '.$author['User']['position'].'</em></p>';
	echo '<p>'.$story['Story']['content'].'</p>';
	if ($story['Story']['modified'] != $story['Story']['created'])
	{
		echo '<p><em>Last updated: '.$story['Story']['modified'].'</em></p>';
	} 
	
	if ($this->Session->check('User.isAdmin'))
		{
			echo '<p><strong>'.$html->link('Edit', '/stories/edit/'.$story['Story']['id']).' '.
			$html->link('Delete', '/stories/delete/'.$story['Story']['id']).'</strong></p>';
		} ?>
	<h3><a href="/stories/">Back</a></h3>
</div>
<div id="sidebar">
</div>
