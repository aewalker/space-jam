<?php
	/*********************************************************************************************
	 * stories/edit.ctp                                                                          *
	 *                                                                                           *
	 * Edit a news story.                                                                        *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Jul 25, 2010                                                 Last updated Jul 25, 2010 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17.                                *
	 *********************************************************************************************/
$this->set("title_for_layout", array('title' => 'Edit Story'));
?><div id="content">
	<?php if ($this->Session->check('User.isAdmin'))
	{
		if (!isset($result))
		{ //Form wasn't filled out yet
			$story = $story['Story'];
			echo $form->create('Story', array('url' => '/stories/edit/'.$story['id']));
			echo $form->text('Story.title', array('cols' => 70, 'value' => $story['title']));
			echo '<br/>';
			echo $form->textarea('Story.content', array('rows' => 20, 'cols' => 75, 'value' => $story['content']));
			echo $form->hidden('Story.id', array('value' => $story['id']));
			echo $form->submit('Edit Story');
			echo $form->end();
			echo '<h3>'.$html->link('Back', '/stories/').'</h3>';
		}
		else
		{ //Form was submited
			if ($result === true)
			{ //Everything worked
				echo '<h3>Success!  Story changed.</h3>';
				echo '<h3>'.$html->link('Back', '/stories/').'</h3>';
			}
			else
			{ //There were problems
				$count = count($result);
				if ($count == 1)
				{
					echo '<h3>Failure!  There was an error:</h3>';
				}
				else
				{
					echo '<h3>Failure!  There were errors:</h3>';
				}
				echo '<div id="error">';
				foreach ($result as $field => $message)
				{
					echo '<p>'.ucwords($field).': '.$message.'</p>';
				}
				echo '</div>';
				echo $html->link('Try Again', '/stories/edit/'.$id);
				echo '<h3>'.$html->link('Back', '/stories/').'</h3>';
			}
		}
	}
	else
	{
		echo '<h3>Only admins may edit stories.</h3>';
		echo '<h3>'.$html->link('Back', '/stories/').'<h3>';
	} ?>
</div>
<div id="sidebar">
	<h1>Edit a news story</h1>
</div>
