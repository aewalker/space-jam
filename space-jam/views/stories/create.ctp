<?php
	/*********************************************************************************************
	 * stories/create.ctp                                                                        *
	 *                                                                                           *
	 * Create a new news story.                                                                  *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Jul 25, 2010                                                 Last updated Jul 25, 2010 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17.                                *
	 *********************************************************************************************/
$this->set("title_for_layout", array('title' => 'Create Story'));
?><div id="content">
	<?php if ($this->Session->check('User.isAdmin'))
	{
		if (!isset($result))
		{ //Form wasn't filled out yet
			echo $form->create('Story', array('url' => '/stories/create/'));
			echo $form->text('Story.title', array('cols' => 70));
			echo '<br/>';
			echo $form->textarea('Story.content', array('rows' => 20, 'cols' => 75));
			echo $form->submit('Create Story');
			echo $form->end();
			echo '<h3>'.$html->link('Back', '/stories/').'<h3>';
		}
		else
		{ //Form was submited
			if ($result === true)
			{ //Everything worked
				echo '<h3>Success!  Story created.</h3>';
				$html->link('Back', '/');
			}
			else
			{ //There were problems
				$count = count($result);
				if ($count == 1)
				{
					echo '<h3>Failure!  There was an error:</h3>';
				}
				else
				{
					echo '<h3>Failure!  There were errors:</h3>';
				}
				echo '<div id="error">';
				foreach ($result as $field => $message)
				{
					echo '<p>'.ucwords($field).': '.$message.'</p>';
				}
				echo '</div>';
				echo $html->link('Try Again', '/stories/create/');
				$html->link('Back', '/stories/');
			}
		}
	}
	else
	{
		echo '<h3>Only admins may create stories.</h3>';
		echo '<h3>'.$html->link('Back', '/stories/').'<h3>';
	} ?>
</div>
<div id="sidebar">
	<h1>Write a new news story</h1>
</div>
