<?php
	/*********************************************************************************************
	 * contacts.ctp                                                                              *
	 *                                                                                           *
	 * Displays contact info for people working at Space Jam.                                    *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Jul 30, 2010                                                 Last updated Aug 22, 2011 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17.                                *
	 *********************************************************************************************/
$this->set("title_for_layout", array('title' => 'Contacts'));
$this->set('currentNavButton', 'Contacts');
?><div id="content">
	<h2>Mission Control</h2>Space Jamboree Mission Control<br/><a href="mailto:mission.control@spacejamboree.com">mission.control@spacejamboree.com</a><br/><br/>
	<h2>Event Organizers</h2>Denny Anderson, Troop 17<br/>#1 Willowbrook Court, Champaign, IL  61820<br/><a href="tel:2178407424">217-840-7424</a><br/><a href="mailto:dashangrila@yahoo.com">dashangrila@yahoo.com</a><br/><br/>
	<a href="http://www.facebook.com/briankaywalker">Brian Walker</a>, Crew 272<br/>1458 N 700 E, Veedersburg, IN  47987<br/><a href="tel:7652951548">765-295-1548</a><br/><a href="mailto:brian@spacejamboree.com">brian@spacejamboree.com</a><br/><br/><br/>
	<h2>Webmaster</h2><a href="http://princeton.edu/~aewalker/" target="_blank">Austin Walker</a>, Crew 272<br/><a href="mailto:webmaster@spacejamboree.com">webmaster@spacejamboree.com</a><br/><br/>
	<h3><a href="/">Back</a></h3>
</div>
<div id="sidebar">
	<h1>Contacts</h1>
	<h3>Questions?  Comments?  Need help?<br />Take a look here to find someone to talk to:</h3>
</div>
