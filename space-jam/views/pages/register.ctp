<?php
	/*********************************************************************************************
	 * register.ctp                                                                              *
	 *                                                                                           *
	 * The actual registration page.                                                             *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Dec 26, 2011                                                 Last updated Jan 07, 2012 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17.                                *
	 *********************************************************************************************/
	$this->set("title_for_layout", array('title' => 'Register'));
	$html->script('https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js', array('inline' => false));
	$html->script('https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js', array('inline' => false));
	$html->script('models', array('inline' => false));
	$html->script('data', array('inline' => false));
	$html->script('actions', array('inline' => false));
	$html->css('reg', 'stylesheet', array('inline' => false));
	$html->css('blitzer.css', 'stylesheet', array('inline' => false));
?>
<div id="content"></div>
<div id="sidebar">
	<div id="schedule_div">
		<table id="schedule_table">
	    	<thead>
	        </thead>
	    	<tbody>
	        </tbody>
	    </table>
	</div>
	<div id="controls_div">
	(1) Name: <input type="text" id="new_scout_name_input"></input>
	(2) Type: <select id="new_scout_type_input"></select>
	(3) <button id="new_scout_button"></button>
	</div>
	<div id="reg_table_div">
		<ul></ul>
	</div>
</div>
