<?php
	/*********************************************************************************************
	 * home.ctp                                                                                  *
	 *                                                                                           *
	 * The main Space Jamboree page.                                                             *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Jul 30, 2010                                                 Last updated Oct 14, 2011 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17.                                *
	 *********************************************************************************************/
	$this->set("title_for_layout", array('title' => 'Home'));
//	$this->set('currentNavButton', 'Home');
	if (time() < SPACEJAM_END) //Only add the countdown script if SpaceJam is not over
							   //This time can be updated in /space_jam/config/bootstrap.php
		$html->script('count', array('inline' => false));
</script> 
?>
<div id="content">
	<h1>Welcome!</h1>
	<h3>to spacejamboree.com, the home of the Space Jamboree Scout Merit Badge Workshop</h3>
	<h2><?php echo $html->link('Register Now!', '/pages/registration/', array('title' => 'Registration Page'));?></h2>
	<img src="/img/kids.jpg" alt="Greetings from Space Jam!" title="Greetings from Space Jam!"/>
	<h2>Latest News:</h2>
	<?php $count = count($news);
	for ($i = 0; $i < $count; $i++)
	{
		echo '<h3>'.$html->link($news[$i]['Story']['title'], '/stories/view/'.$news[$i]['Story']['id'], array(
					'title' => $news[$i]['Story']['title'])).'</h3>';
	} ?>
</div>
<div id="sidebar">
	<a href="http://www.facebook.com/pages/Space-Jamboree/137761722911346" target="_blank"><img src="/img/facebook-logo.png" width="64px" height="64px" /></a>
	<h3>Space... the Final Frontier</h3>
	<p>We invite you to go boldly where no troop has gone before, seek out new and high tech Merit Badges: Space Exploration, Aviation, Radio, and Electronics to name a few. Space Jam is the scouting technology event of the year. Registered Scouts have a chance to talk with astronauts on the International Space Station, so get your registration in early. Tons of high tech merit badges, air conditioned class rooms, a live video conference with NASA, a high altitude balloon launch, Guitar Hero and DDR contests, music, robotics, food vendors, camp sites, and more!
	</p>
	<?php if (time() < SPACEJAM_END)
	{ ?>
		<h3>Time Until SJ-6:</h3>
		<span id="countdown"></span>
		<?php }
	?>
</div>
