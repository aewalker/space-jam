<?php
	/*********************************************************************************************
	 * sponsors.ctp                                                                              *
	 *                                                                                           *
	 * Shows information about our spnonsors and has a donation button.                          *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Feb 03, 2011                                                 Last updated Oct 14, 2011 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17.                                *
	 *********************************************************************************************/
	$this->set("title_for_layout", array('title' => 'Fees and Sponsors'));
	$this->set('currentNavButton', 'Fees/Sponsors');
?>
<div id="content">
	<p><strong>To the area community, individuals, organizations and businesses:</strong></p>

	<p>SPACE JAM 6 (Aug. 3, 4 &amp; 5, 2012) is Boy Scout weekend event open to
	other organizations like the Girl Scouts and Don Moyers Boys and Girls
	Club. We talk live with the astronauts on the International Space
	Station, work on 30 merit badges, including Robot construction,
	Aviation, Computers, Space Exploration, Emergency Preparedness,
	Medicine and Introduction to Scuba Diving. Two Scout units, Troop 17
	and Crew 272, provide the core of volunteers with the help of more
	than a hundred volunteers from other units. The action packed weekend
	concludes with an actual Airport Disaster Drill at the Rantoul Airport
	that we rent for the event.</p>

	<p>Scouting is widely known for developing pioneer skills like fire
	building and knot tying and always will, but the new pioneers are on
	the space station and the new skills needed are the core of our
	program, technology skills. We need your help to not only continue but
	expand our program.  Please help at whatever level of support you can.</p>

	<p>We have not raised our $25 fee for 4 years preferring to make the
	experience available to as many as possible and although most of the
	merit badges have costs associated, we ask no additional fees (with
	the exception of Robotics). While the costs of airport rental, museum
	access, porta-pottys, dumpster, tents and the hundreds other expenses
	have all increased, we are committed to not raising fees but the only
	way we can do this is with your help. We will have up to 2000 Scouts
	and Adults in attendance this year and need to raise an additional
	$2,000/3,000. Please help with this event in any capacity that you
	can. Donations of $250 or more by organizations/businesses will be
	listed as Sponsors on our brochures and web-site.</p>

	<p>Please send your donations to "Space Jam" (PayPal  or mail to Troop
	17), Thank You very much!</p>

	<p>Space Jam c/o Troop 17<br />
	Denny Michael Anderson<br />
	#1 Willowbrook Court<br />
	Champaign, IL 61820</p>
	
	<p>Call Denny at <a href="tel:2178407424">217-840-7424</a> with questions</p>
	
	<p>If it wasn't for the help and support of the organizations and people listed here,
	Space Jamboree would have never become the event that it is today.  We are very greatful
	for all that they have done.</p>
	<?php
	foreach ($sponsors->sponsors as $sponsor)
	{
		echo $html->link($sponsor['name'], $sponsor['site'], array('target' => '_blank'));
		echo '<br /><br />';
	}
	?>
</div>
<div id="sidebar">
	<h3>Donate via PayPal:</h3>
	<form action="https://www.paypal.com/cgi-bin/webscr" target="_blank" method="post">
	<input type="hidden" name="cmd" value="_s-xclick">
	<input type="hidden" name="hosted_button_id" value="ZML8UKZ3ZUXZY">
	<input type="image"
	src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif"
	border="0" name="submit" alt="PayPal - The safer, easier way to pay
	online!">
	<img alt="" border="0"
	src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1"
	height="1">
	</form>
</div>
