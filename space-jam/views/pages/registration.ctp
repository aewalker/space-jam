<?php
	/*********************************************************************************************
	 * registration.ctp                                                                          *
	 *                                                                                           *
	 * Contains registration info and forms, plus schedule and advirtising information.          *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Jul 30, 2010                                                 Last updated Jan 02, 2012 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17.                                *
	 *********************************************************************************************/
	$this->set("title_for_layout", array('title' => 'Registration'));
	$this->set('currentNavButton', 'Register');
?>
<div id="content">
	<h3>Welcome to the Registration Page!</h3>
	<p>To register for Space Jamboree, download either the Scout (Boy and Girl Scouts) flyer or the Webelos flyer on the left.  Print out the flyer and mail it in along with payment.  Prices and mailing address are included on the flyers.  Also print out a copy of either the merit badge schedule or the Webelos pin classes schedule for each scout/Webleos and have him/her select classes.  Cirlce the desired classes and send the schedules in along with the main registration form.  If you have questions about registration, contact Denny Anderson (see the <?php echo $html->link('Contacts Page', '/pages/contacts/', array('title' => 'Contacts page')); ?>).</p>
    <p>Online registration will hopefully be up soon; for now please use the printable forms.</p>
	<p>If you are looking to promote Space Jam to your unit or at a roundtable, you can use the 'Overview' or 'Roundtable' flyers.</p>
</div>
<div id="sidebar">
	<h3>Registration</h3>
	<?php
		echo $html->link('Scout Registration','/files/SJ6_registration_Scoutsa.pdf',
			array('title' => 'Download the Scout registration form', 'target' => '_blank'));
		echo '<br />';
		echo '<br />';
		echo $html->link('Webelos Registration', '/files/SJ6 registration Webelos.pdf',
			array('title' => 'Download the Webelos registration form', 'target' => '_blank'));
		echo '<br />';
		echo '<br />';
		echo $html->link('Talent Release', '/files/talentreleasesj6.pdf',
			array('title' => 'Download the talent release form', 'target' => '_blank'));
		echo '<br />';
		echo '<br />';
		echo $html->link('Group Talent Release', '/files/talentreleasesj6group.pdf',
			array('title' => 'Download the group talent relase form', 'target' => '_blank'));
	?>
	<h3>Schedule</h3>
	<?php 
		echo $html->link('Main Schedule', '/files/SPACE JAM 6 SCHEDULE of Activities.pdf',
			array('title' => 'Download the tentative event schedule', 'target' => '_blank'));
		echo '<br />';
		echo '<br />';
		echo $html->link('Merit Badge Classes Schedule', '/files/Space_Jam_6_Schedule_Spreadsheet.pdf',
			array('title' => 'Download the tentative merit badge class schedule', 'target' => '_blank'));
		echo '<br />';
		echo '<br />';
		echo $html->link('Webelos Pin Classes Schedule', '/files/Space Jam 6 Webelos Schedule Spreadsheet.pdf',
			array('title' => 'Download the tentative Webelos pin classes schedule', 'target' => '_blank'));
		echo '<br />';
		echo '<br />';
		echo $html->link('A note for Robotics MB class', '/files/robotics_intro.pdf',
			array('title' => 'Special notice for the Robotics MB class', 'target' => '_blank'));
		echo '<br />';
		echo '<br />';
		echo $html->link('Release for airplane rides', '/files/Young_Eagles_form.pdf',
			array('title' => 'Release form for airplane rides', 'target' => '_blank'));
		echo '<br />';
		echo '<br />';
		echo $html->link('Merit Badge Prerequisites', '/files/prerequisites.pdf',
			array('title' => 'Merit Badge Prerequisites', 'target' => '_blank'));
	?>
	<h3>Other Flyers</h3>
	<?php
		/*echo $html->link('Merit Badge Prerequisites', '/files/prerequisites.pdf',
			array('title' => 'Download SJ merit badge class prerequisites information', 'target' => '_blank'));
		echo '<br />';
		echo '<br />';*/
		echo $html->link('Overview Flyer', '/files/SJ6 Overview Flyer.pdf',
			array('title' => 'Download the SJ advirtiing flyer', 'target' => '_blank'));
		echo '<br />';
		echo '<br />';
		echo $html->link('Roundtable Flyer', '/files/Space Jam 6 Roundtable flier.pdf',
			array('title' => 'Download the SJ roundtable flyer', 'target' => '_blank'));
	?>
</div>
