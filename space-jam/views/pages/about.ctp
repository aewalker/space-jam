<?php
	/*********************************************************************************************
	 * about.ctp                                                                                 *
	 *                                                                                           *
	 * Information about Space Jam.                                                              *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Jul 30, 2010                                                 Last updated Oct 14, 2011 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17.                                *
	 *********************************************************************************************/
$this->set("title_for_layout", array('title' => 'About'));
$this->set('currentNavButton', 'About');
?><div id="content">
	<h1>What is Space Jamboree?</h1>
	<p>The Space Jamboree Workshop (or just Space Jam for short), is the midwest's largest technology oriented gathering of all Boy Scouts and Girl Scouts.  Put on by Crew 272 of Fountain County, IN, and Troop 17 of Champaign, IL, Space Jam is about fun with a purpose: we would love to see Scouts go on to become scientists, engineers, entrepreneurs, teachers, musicians, technicians, and much more. We want to continue and develop excellence in teaching and share what we learn.  Boy Scouts, Girl Scouts, and Webelos are all welcome.</p>
	<p>Space Jam 6 takes place August 3-5 at the <?php echo $html->link('Chanute Air Museum', 'http://www.aeromuseum.org/', array('title' => 'Chanute Air Museum Website', 'target' => '_blank')); ?> in Rantuol, IL.  Here's a list of the activities planned for this year.  This year's featured merit badge will be Aviation in honor of our theme "The History of Aviation". New activities are in bold:</p>
	<table bgcolor="#E8E8E8" width="540">
<!-- OFFICIAL MERIT BADGES -->
<tr><td colspan="3"><center><strong>Offical Merit Badges</strong></center></td></tr>

<tr><td width="180"><center>Archery</center></td><td width="180"><center>Aviation</center></td><td width="180"><center><strong>Chess</center></td></tr>

<tr><td width="180"><center>Cit. in the World</center></td><td width="180"><center>Computers</center></td><td width="180"><center>Disabilities Aware.</center></td></tr>

<tr><td width="180"><center>Electronics</center></td><td width="180"><center>Emergency Prep.</center></td><td width="180"><center>Energy</center></td></tr>

<tr><td width="180"><center>Environmental Sci.</center></td><td width="180"><center>Fingerprinting</center></td><td width="180"><center>Fire Safety</center></td></tr>

<tr><td width="180"><center>Geocaching</center></td><td width="180"><center>Geology</center></td><td width="180"><center><strong>Inventing</strong></center></td></tr>

<tr><td width="180"><center>Medicine</center></td><td width="180"><center>Metalwork</center></td><td width="180"><center>Pulp &amp; Paper</center></td></tr>

<tr><td width="180"><center>Radio</center></td><td width="180"><center>Robotics</center></td><td width="180"><center>Scholarship</center></td></tr>

<tr><td width="180"><center>Soil &amp; Water Cons.</center></td><td width="180"><center>Space Exploration</center></td><td width="180"><center>Textiles</center></td></tr>

<tr><td width="180"><center>Truck Trans.</center></td><td width="180"></td><td width="180"><center>Woodworking</center></td></tr>

</table><br>

<!-- WEBELOS ACTIVITY PINS -->
<table bgcolor="#E8E8E8" width="540">

<tr><td colspan="3"><center><strong>Webelos Activity Pins</strong></center></td></tr>

<tr><td width="180"><center>Readyman</center></td><td width="180"><center>Engineer</center></td><td width="180"><center>Scientist</center></td></tr>

</table><br>

<!-- OTHER ACTIVITIES -->
<table bgcolor="#E8E8E8" width="540">

<tr><td colspan="3"><center><strong>Other Activities</strong></center></td></tr>

<tr><td width="180"><center>International Space Station Radio Contact</center></td><td width="180"><center>Live NASA Digital Learning Network Video Contact</center></td><td width="180"><center>Balloon Launch</center></td></tr>

<tr><td width="180"><center>Amateur ("Ham") Radio</center></td><td width="180"><center>Search and Rescue Demonstration</center></td><td width="180"><center>Archery &amp; BB Gun Range</center></td></tr>

<tr><td width="180"><center>Inflatables</center></td><td width="180"><center>Movie Night</center></td><td width="180"><center>Chanute Air Museum Self-Guided Tours</center></td></tr>

<tr><td width="180"><center>Guitar Hero Contest</center></td><td width="180"><center>Discover SCUBA</center></td><td width="180"><center>Duck Tape!</center></td></tr>

</table>

<p>Note that some merit badges do have prerequisites that cannot be fulfilled at Space Jam.  You can downlaod information about those prerequisites here: <?php echo $html->link('Merit Badge Prerequisites', '/files/prerequisites.pdf',
			array('title' => 'Download SJ-6 merit badge class prerequisites information', 'target' => '_blank')); ?></p>

<p>As August gets closer, class and event schedules will be posted.</p>
<?php /* <p>In addition, here are the current events schedule and merit badge class schedules.  These are bound to change; the most current version will be availible at Space Jam:</p>
<ul>
	<li><?php echo $html->link('Main Schedule', '/files/tentative_schedule.pdf',
			array('title' => 'Download the tentative event schedule', 'target' => '_blank')); ?></li>
	<li><?php echo $html->link('Merit Badge Classes Schedule', '/files/tentative_class_schedule.pdf',
			array('title' => 'Download the tentative merit badge class schedule', 'target' => '_blank')); ?></li>
</ul> */ ?>

<p>Included in the cost of of attendance is access to a campsite area.  Groups are welcome to camp Friday and Saturday night.</p>
<h4>Space Jamboree Map</h4>
<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?q=40.294323,-88.147902&amp;num=1&amp;t=h&amp;sll=40.294323,-88.147902&amp;sspn=0.012373,0.01929&amp;hl=en&amp;ie=UTF8&amp;rq=1&amp;split=0&amp;ll=40.293995,-88.146358&amp;spn=0.012373,0.01929&amp;z=14&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?q=40.294323,-88.147902&amp;num=1&amp;t=h&amp;sll=40.294323,-88.147902&amp;sspn=0.012373,0.01929&amp;hl=en&amp;ie=UTF8&amp;rq=1&amp;split=0&amp;ll=40.293995,-88.146358&amp;spn=0.012373,0.01929&amp;z=14&amp;source=embed" target="_blank" style="color:#0000FF;text-align:left">View Larger Map</a></small>
</div>
<div id="sidebar">
	<h2>Space Jamboree Mission Statement:</h2>
	<p>Space Jam is all about embracing the future... Having fun, that is fun with a purpose! We would love to see Scouts go on to become scientists, engineers, entrepreneurs, teachers, musicians, and technicians. We want to continue and develop excellence in merit badge teaching and share what we learn.</p>
</div>
