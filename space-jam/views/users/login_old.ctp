<?php
	/*********************************************************************************************
	 * users/login.ctp                                                                           *
	 *                                                                                           *
	 * Page to display after a login atempt.                                                     *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Jul 26, 2010                                                 Last updated Jul 27, 2010 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17.                                *
	 *********************************************************************************************/
$this->set("title_for_layout", array('title' => 'Login', 'description' => 'Admin Login'));
?><div id="content">
	<?php if ($result == 'success')
	{
		echo '<h3>Success!  You are now loged in.</h3>';
		echo '<h3>'.$html->link('Back', '/').'</h3>';
	}
	else if ($result == '')
	{
		echo '<h3>Admin Login</h3>';
		echo '<p>Please log in.</h3>';
		echo $form->create('User', array('url' => '/users/login/', 'div' => false)).
				'Username: '.
				$form->text('User.username', array('div' => false)).' Password: '.
				$form->password('User.password', array('div' => false)).' '.
				$form->submit('Login', array('div' => false)).
				$form->end();
	}
	else
	{
		echo '<h3>Failure!  Could not log in.</h3>';
		echo '<h3>'.$html->link('Back', '/').'</h3>';
	} ?>
	
</div>
