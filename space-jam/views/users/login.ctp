<?php
	/*********************************************************************************************
	 * users/login.ctp                                                                           *
	 *                                                                                           *
	 * Page to display after a login atempt.                                                     *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Jul 26, 2010                                                 Last updated Jul 27, 2010 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17.                                *
	 *********************************************************************************************/
$this->set("title_for_layout", array('title' => 'Login', 'description' => 'Admin Login'));
?><div id="content">
	<?php
		echo $this->Session->flash('auth');
		echo $this->Form->create('User');
		echo $this->Form->input('username');
		echo $this->Form->input('password');
		echo $this->Form->end('Login');
		echo '<p>New? '.$html->link('Register now!', array('controller' => 'users', 'action' => 'chooseType')).'</p>'
	?>
	
</div>
