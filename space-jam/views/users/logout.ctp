<?php
	/*********************************************************************************************
	 * users/logout.ctp                                                                          *
	 *                                                                                           *
	 * Page to show that the user has logout out.                                                *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Jul 26, 2010                                                 Last updated Jul 27, 2010 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17.                                *
	 *********************************************************************************************/
$this->set("title_for_layout", array('title' => 'Logout', 'description' => 'Admin Logout'));
?><div id="content">
	<h3>You have been logged out.</h3>
	<h3><?php echo $html->link('Back', '/'); ?></h3>
</div>
