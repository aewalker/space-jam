<?php
	/*********************************************************************************************
	 * users/choose_type.ctp                                                                     *
	 *                                                                                           *
	 * Allows visitors to pick which type of account to register.                                *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Nov 02, 2011                                                 Last updated Nov 02, 2011 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17.                                *
	 *********************************************************************************************/
$this->set("title_for_layout", array('title' => 'Site Registration', 'description' => 'Register a new user'));
?><div id="content">
	<?php
		echo $form->create('User', array('action' => 'chooseType'));
		echo $form->input('typeofuser', array('label' => 'Select registration option', 'type' => 'select', 'options' => array(
			'personal' => 'I have a regestration code',
			'reguser' => 'I want to register my unit'
		)));
		echo $form->submit();
		echo $form->end();
	?>
</div>
<div id="sidebar">
	<h1>Register for Space Jam</h1>
</div>
