<?php
	/*********************************************************************************************
	 * users/give_code.ctp                                                                       *
	 *                                                                                           *
	 * Allows new users to give a regestration code.                                             *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Nov 02, 2011                                                 Last updated Nov 02, 2011 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17.                                *
	 *********************************************************************************************/
$this->set("title_for_layout", array('title' => 'Site Registration', 'description' => 'Register a new user'));
?><div id="content">
	<?php
		echo $form->create('User', array('action' => 'giveCode'));
		echo '<p>Please enter the regestration code given to you.</p>';
		echo $form->input('code', array('label' => ''));
		echo $form->submit();
		echo $form->end();
		echo '<h3>'.$html->link('Back', '/').'</h3>';
	?>
</div>
<div id="sidebar">
	<h1>Register for Space Jam</h1>
</div>
