<?php
	/*********************************************************************************************
	 * users/codeerr.ctp                                                                         *
	 *                                                                                           *
	 * Shown when a bad regestartion code is given.                                              *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Nov 02, 2011                                                 Last updated Nov 02, 2011 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17.                                *
	 *********************************************************************************************/
$this->set("title_for_layout", array('title' => 'Site Registration', 'description' => 'Register a new user'));
?><div id="content">
	<p>The regestration code you gave was not valid.  Please try again.</p>
	<?php
		echo '<h3>'.$html->link('Back', '/').'</h3>';
	?>
</div>
<div id="sidebar">
	<h1>Register for Space Jam</h1>
</div>
