<?php
	/*********************************************************************************************
	 * users/register.ctp                                                                        *
	 *                                                                                           *
	 * Allows new users to register for a login name.                                            *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Jul 27, 2010                                                 Last updated Jul 27, 2010 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17.                                *
	 *********************************************************************************************/
$this->set("title_for_layout", array('title' => 'Site Registration', 'description' => 'This page currently inactive'));
?><div id="content">
	<?php /* Old version that would allow people to sign up.
	if ($this->Session->check('User'))
	{
		echo '<h3>You are already logged in, please '.$html->link('log out', '/users/logout').
		' before creating a new account.</h3>';
	}
	else
	{ 
		if (!isset($result))
		{
			echo '<h1>spacejamboree.com Sign Up</h1>'.
			'<h2>Please fill out all fields</h2>'.
			$form->create('User', array('url' => '/users/register/')).
			'<p>Username</p>'.$form->text('User.username').
			'<p>Password</p>'.$form->password('User.password').
			'<p>Confirm Password</p>'.$form->password('User.confirm').
			'<br/><br/>'.$form->submit('Sign Up').$form->end();
		}
		else
		{
			if ($result === true)
			{
				echo '<h3>Success!  You can now log in.</h3>';
			}
			else
			{
				$count = count($result);
				if ($count == 1)
				{
					echo '<h3>Failure!  There was an error:</h3>';
				}
				else
				{
					echo '<h3>Failure!  There were errors:</h3>';
				}
				echo '<div id="error">';
				foreach ($result as $field => $message)
				{
					echo '<p>'.ucwords($field).': '.$message.'</p>';
				}
				echo '</div>';
				echo $html->link('Try Again', '/users/register/');
			}
		}
	}*/
	echo '<h3>Sorry! User registration is disabled at this time.</h3>';
	echo '<h3>'.$html->link('Back', '/').'</h3>'; ?>
</div>
<div id="sidebar">
	<h1>Register for a username</h1>
</div>
