<?php
	/*********************************************************************************************
	 * sponsors.php                                                                              *
	 *                                                                                           *
	 * Helper to encapsulate information about our sponsors.  Enabled in the pages controller.   *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Feb 22, 2011                                                 Last updated Oct 09, 2011 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17.                                *
	 *********************************************************************************************/
	class SponsorsHelper extends Helper
	{
		public $sponsors = array
		(
			array
			(
				'name' => 'AAA Storage',
				'site' => 'http://www.aaa-storage.com/',
				'img' => 'aaa-storage.png'
			),
			array
			(
				'name' => 'Absopure Water Corporation',
				'site' => 'http://www.absopure.com/',
				'img' => 'absopure.jpg'
			),
			array
			(
				'name' => 'Action Inflatables Mega Events',
				'site' => 'http://www.aimegaevents.com/',
				'img' => 'action-inflatables.png'
			),
			array
			(
				'name' => 'US Army Corp of Engineers',
				'site' => 'http://www.usace.army.mil/',
				'img' => 'army-engineers.png'
			),
			array
			(
				'name' => 'Central Illinois Aerospace',
				'site' => 'http://www.ciarocketry.org/',
				'img' => 'central-illinois-aerospace.png'
			),
			array
			(
				'name' => 'Chanute Air Museum',
				'site' => 'http://www.aeromuseum.org',
				'img' => 'chanute-air-museum.png'
			),
			array
			(
				'name' => 'Chief A J',
				'site' => 'http://www.chiefaj.com/',
				'img' => 'chief-a-j.png'
			),
			array
			(
				'name' => 'Coastal ChipWorks',
				'site' => 'http://www.coastalchip.com/',
				'img' => 'coastal-chipworks.png'
			),
			array
			(
				'name' => 'Dean\'s Superior Blueprint',
				'site' => 'http://www.deansblueprint.com',
				'img' => 'deans-superior-blueprint.jpg'
			),
			array
			(
				'name' => 'DePauw University',
				'site' => 'http://www.depauw.edu/',
				'img' => 'depauw.png'
			),
			array
			(
				'name' => 'Don Moyer Boys and Girls Club',
				'site' => 'http://www.dmbgc-cu.org/',
				'img' => 'don-moyer-club.png'
			),
			array
			(
				'name' => 'First Federal Savings Bank of Champaign-Urbana',
				'site' => 'http://www.356bank.com/',
				'img' => 'first-federal.png'
			),
			array
			(
				'name' => 'Fountain Foundry Corporation',
				'site' => 'http://www.fountainfoundry.com/',
				'img' => 'fountain-foundry.png'
			),
			array
			(
				'name' => 'Gotta Potty',
				'site' => 'http://www.gottapottyrentals.com/',
				'img' => 'gotta-potty.jpg'
			),
			array
			(
				'name' => 'The Izaak Walton Leauge of America',
				'site' => 'http://www.iwla.org/',
				'img' => 'izaak-walton-league.png'
			),
			array
			(
				'name' => 'The Moritz Embrodery Works, Inc.',
				'site' => 'http://www.moritzembroidery.com/',
				'img' => 'moritz-embrodery.png'
			),
			array
			(
				'name' => 'NASA',
				'site' => 'http://www.nasa.gov/',
				'img' => 'nasa.png'
			),
			array
			(
				'name' => 'Oscar Winski',
				'site' => 'http://www.oscarwinski.com/',
				'img' => 'oscar-winski.png'
			),
			array
			(
				'name' => 'The Quarters Inn and Suites',
				'site' => 'http://thequartersinn.biz/',
				'img' => 'quarters-inn.gif'
			),
			array
			(
				'name' => 'Rental City',
				'site' => 'http://www.cuatrentalcity.com/',
				'img' => 'rental-city.png'
			),
			array
			(
				'name' => 'Segway',
				'site' => 'http://www.segway.com/',
				'img' => 'segway.png'
			),
			array
			(
				'name' => 'SewVac Central',
				'site' => 'http://www.sewvaccentral.com/',
				'img' => 'sewvac.jpg'
			),
			array
			(
				'name' => 'Shurtech',
				'site' => 'http://shurtech.com/',
				'img' => 'shurtech.jpg'
			),
			array
			(
				'name' => 'TC Tatman General Contractors',
				'site' => 'http://www.tctgc.com/',
				'img' => 'tc-tatman.png'
			),
			array
			(
				'name' => 'Thrivent Financial for Lutherans',
				'site' => 'https://www.thrivent.com/',
				'img' => 'thrivent-financial.png'
			),
			array
			(
				'name' => 'Tommy House Tire Co.',
				'site' => 'http://www.tommyhousetire.com/',
				'img' => 'tommy-house.png'
			),
			array
			(
				'name' => 'Tru3D',
				'site' => 'http://tru3d.com/',
				'img' => 'tru3d.jpg'
			),
			array
			(
				'name' => 'Weiskamp',
				'site' => 'http://www.weiskamp.com/',
				'img' => 'weiskamp-ts.jpg'
			)
		);
	}
?>
