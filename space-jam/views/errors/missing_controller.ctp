<?php
	/*********************************************************************************************
	 * missing_controller.ctp                                                                    *
	 *                                                                                           *
	 * Message to display when an invalid url is entered.                                        *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Jul 25, 2010                                                 Last updated Feb 03, 2011 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17.                                *
	 *********************************************************************************************/
$this->set("title_for_layout", array('title' => 'Error'));
?><div id="content">
	<h1>Error</h1>
	<p>The page you have requested does not exist.<br/>
	If you came to this page from a link on the Space Jamboree website, please <a
	href="mailto:webmaster@spacejamboree.com">email the webmaster</a>.</p>
	<p><?php echo $html->link('Back', WWW_ROOT); ?></p>
</div>
