<?php
	/*********************************************************************************************
	 * app_controller.php                                                                        *
	 *                                                                                           *
	 * Parent controller for all the other controllers.                                          *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Jul 26, 2010                                                 Last updated Jul 26, 2010 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America crew 272 and Troop 17.                                *
	 *********************************************************************************************/
	class AppController extends Controller
	{
		/* if php4
		 *var $name = 'app';
		 */
		public $helpers = array('Html', 'Form', 'Session');
		public $components = array('Ssl', 'Session');
		
		function beforeFilter()
		{
			//Uncomment this to force all connections to be secured.
			//$this->Ssl->force();
		}
	}
?>
