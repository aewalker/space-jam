<?php
	/*********************************************************************************************
	 * badges_controller.php                                                                     *
	 *                                                                                           *
	 * Controls the badges (classes) that scouts can register for.                               *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Jan 05, 2012                                                 Last updated Jan 05, 2012 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272.                                             *
	 *********************************************************************************************/
	class BadgesController extends AppController
	{
		var $components = array('RequestHandler');
		/**
		 * Gets a full JSON listing of all badges.
		 */
		function getAllBadges()
		{
			$fields = array('id', 'name', 'cat', 'loc', 'section', 'hour', 'duration', 'size', 'restrictions', 'fee', 'note');
			$badges = $this->Badge->find('all', array('fields' => $fields, 'recursive' => 0));
			$next = 0;
			$json = array();
			foreach ($badges as $badge)
			{
				$nextBadge = array();
				foreach ($fields as $field)
				{
					$nextBadge[$field] = $badge['Badge'][$field];
				}
				$json[$next] = $nextBadge;
				$next++;
			}
			$this->set('results', json_encode($json));
			$this->render('ajax-response');
		}
	}
