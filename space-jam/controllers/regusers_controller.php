<?php
class RegusersController extends AppController {

	var $name = 'Regusers';
	var $helpers = array('Html', 'Form');

	function index() {
		$this->Reguser->recursive = 0;
		$this->set('regusers', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Reguser.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('reguser', $this->Reguser->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Reguser->create();
			if ($this->Reguser->save($this->data)) {
				$this->Session->setFlash(__('The Reguser has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Reguser could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Reguser', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Reguser->save($this->data)) {
				$this->Session->setFlash(__('The Reguser has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Reguser could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Reguser->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Reguser', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Reguser->del($id)) {
			$this->Session->setFlash(__('Reguser deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}


	function y_index() {
		$this->Reguser->recursive = 0;
		$this->set('regusers', $this->paginate());
	}

	function y_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Reguser.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('reguser', $this->Reguser->read(null, $id));
	}

	function y_add() {
		if (!empty($this->data)) {
			$this->Reguser->create();
			if ($this->Reguser->save($this->data)) {
				$this->Session->setFlash(__('The Reguser has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Reguser could not be saved. Please, try again.', true));
			}
		}
	}

	function y_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Reguser', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Reguser->save($this->data)) {
				$this->Session->setFlash(__('The Reguser has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Reguser could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Reguser->read(null, $id);
		}
	}

	function y_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Reguser', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Reguser->del($id)) {
			$this->Session->setFlash(__('Reguser deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

}
?>