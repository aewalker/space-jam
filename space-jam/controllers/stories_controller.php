<?php
	/*********************************************************************************************
	 * stories_controller.php                                                                    *
	 *                                                                                           *
	 * Controller to handle news stories for the homepage and stories page.                      *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Jul 25, 2010                                                 Last updated Feb 03, 2011 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17.                                *
	 *********************************************************************************************/
	class StoriesController extends AppController
	{
		protected $maxContent = 300;
		
		function index()
		{ //Show the first 10 stories
			$results = $this->Story->find('all', array('order' => 'Story.created DESC', 'limit' => 10));
			foreach($results as &$result)
			{
				$result = $result['Story'];
				if (strlen($result['content']) > $this->maxContent)
				{
					$result['content'] = substr($result['content'], 0, $this->maxContent - 4).'...';
				}
			}
			$this->set('stories', $results);
		}
		
		function view($id = null)
		{ //Show a particular story in full;
			if ($id == null)
			{
				$this->index();
				$this->render('index');
			}
			else
			{
				$this->set('story', $this->Story->find('first', array('conditions' => array('Story.id'=> $id))));
				$this->set('author', $this->Story->User->find('first', array('conditions' => array(
				'username' => $this->Story->field('author', array('Story.id' => $id))))));
			}
		}
		
		function create()
		{
			if ($this->Session->check('User.isAdmin'))
			{
				if (!empty($this->data['Story']))
				{ //Form was filled out, so process it.
					$this->data['Story']['author'] = $this->Session->read('User.userName');
					$result = $this->Story->save($this->data);
					if ($result)
					{
						$this->set('result', true);
					}
					else
					{
						$this->set('result', $this->Story->validationErrors);
					}
				}
			}
		}
		
		function edit($id = null)
		{
			if ($this->Session->check('User.isAdmin'))
			{
				if ($id == null)
				{
					$this->set('result', array('Edit' => 'No story selected to edit'));
				}
				else
				{
					if (empty($this->data['Story']))
					{
						$this->set('story',$this->Story->read(array('id', 'title', 'content'), $id));
					}
					else
					{
						$result = $this->Story->save($this->data);
						if ($result)
						{
							$this->set('result', true);
						}
						else
						{
							$this->set('result', $this->Story->validationErrors);
							$this->set('id', $id);
						}
					}
				}
			}
		}
		
		function delete($id = null)
		{
			if ($this->Session->check('User.isAdmin'))
			{
				if ($this->data['Story']['confirm'] == 'true')
				{
					if ($id != null)
					{
						$this->set('result', $this->Story->delete($id));
					}
					else
					{
						$this->set('result', false);
					}
				}
				else
				{
					$this->set('id', $id);
				}
			}
		}
		
		function beforeFilter()
		{
			parent::beforeFilter();
		}
	}
?>
