<?php
	/*********************************************************************************************
	 * pages_controller.php                                                                      *
	 *                                                                                           *
	 * Controls pages (such as the homepage).                                                    *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Jul 30, 2010                                                 Last updated Feb 22, 2011 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17.                                *
	 *********************************************************************************************/
	class PagesController extends AppController
	{
		public $uses = array();
		public $helpers = array('Sponsors');
		
		public function homepage()
		{
			$this->loadModel('Story');
			$news = $this->Story->find('all', array
			(
				'order' => 'Story.created DESC',
				'limit' => 5,
				'fields' => array('title', 'id')
			));
			$this->set('news', $news);
			$this->render('home');
		}
		
		public function display($page)
		{
			$this->pageTitle = ucfirst($page);
			$this->render($page);
		}
	}
?>
