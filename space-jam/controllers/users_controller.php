<?php
	/*********************************************************************************************
	 * users_controller.php                                                                      *
	 *                                                                                           *
	 * Handels user login and sessions.                                                          *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Jul 26, 2010                                                 Last updated Feb 02, 2011 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Toop 17.                                 *
	 *********************************************************************************************/
class UsersController extends AppController
	{
		var $secure = true;
		
		var $components = array('Auth', 'RequestHandler', 'Session');
		var $uses = array('User', 'Reguser', 'Badge');
		
		/*
		 * Standard functions
		 */
		
		/** User control panel; change password, etc. */
		function index()
		{
			
		}
		
		/** log the user in */
		function login()
		{
			//handled by auth
		}
		
		/** log the user out */
		function logout()
		{
			//handled by auth
			$this->redirect($this->Auth->logout());
		}
		
		/** allow a registering user to pick the type of registration to do */
		function chooseType()
		{
			if (empty($this->data)) return;
			if ($this->data['User']['typeofuser'] == 'personal')
				$this->render('giveCode');
			else if ($this->data['User']['typeofuser'] == 'reguser')
				$this->render('reguserReg');
			else
				$this->render('regerr');
		}
		
		/** give a code to register a normal user account */
		function giveCode()
		{
			$code = $this->data['User']['code'];
			if (!is_string($code) || !strlen($code) == CODE_LEN)
			{
				$this->render('codeerr');
				return;
			}
			$reguser_id =  $this->Reguser->field('id', array('code' => $code));
			if ($reguser_id == false)
			{
				$this->render('codeerr');
				return;
			}
			$this->set('reguser_id', $reguser_id);
			$this->render('userReg');
		}
		
		/** register a normal user */
		function userReg()
		{
			if (!empty($this->data))
			{
				$this->data['User']['typeofuser'] = 'personal';
				if ($this->data['User']['password'] == $this->Auth->password($this->data['User']['password_confirm']))
				{
					$this->User->create();
					if ($this->User->save($this->data))
						$this->redirect('/users/login');
				}
			}
		}
		
		/** register a reguser */
		function reguserReg()
		{
			if (!empty($this->data))
			{
				$this->data['User']['typeofuser'] = 'reguser';
				if ($this->data['User']['password'] == $this->Auth->password($this->data['User']['password_confirm']))
				{
					$this->User->create();
					$this->data['User']['reguser_id'] = 0;
					if ($this->User->save($this->data))
					{
						$this->Reguser->create();
						$this->data['Reguser']['user_id'] = $this->User->field('id');
						do
						{
							$randString = '';
							//hacks courtesy of php manual
							for ($i = 0; $i <= CODE_LEN; $i++)
							{
							    $d = rand(1, 30) % 2;
							    $randString = $randString.($d ? chr(rand(65, 90)) : chr(rand(48, 57)));
							}
							$this->data['Reguser']['code'] = $randString;
						}
						while ($this->Reguser->find('count', array('conditions' => array('code' => $this->data['Reguser']['code']))) > 0);
						if ($this->Reguser->save($this->data))
						{
							$this->data['User']['reguser_id'] = $this->Reguser->field('id');allow(array('chooseType', 'giveCode', 'userReg', 'reguserReg'));
							$this->User->save($this->data);
							$this->redirect('/users/login');
						}
					}
				}
			}
		}
		
		/*
		 * AJAX Functions
		 */
		/**
		 * Adds a scout to the current reguser.
		 * 
		 * @param name
		 * @param type one of 'Scout', 'Webelo', 'Scouter', or 'Webelo Leader'
		 * @return the new id, or -1 and the reason if one cannot be made
		 */
		function addScout($name = -1, $type = "")
		{
			if ($type != 'Scout' && $type != 'Webelo' && $type != 'Scouter' && $type != 'Webelo Leader')
			{
				$results = "-1\n\"$type\" is not a valid type.";
			}
			else if (empty($name))
			{
				$retuls = "-1\nYou must provide a name for the new $type.";
			}
			else
			{
				$total = $this->User->find('count');
				if ($total < MAX_PARTICIPANTS)
				{
					$this->User->create();
					$result = $this->User->save(array('User' => array(
						'name' => $name,
						'typeofuser' => $type,
						'reguser_id' => $this->_getReguser())));
					if ($result === false)
					{
						$results = '-1\nDatabase error(s) occured:';
						foreach ($this->User->validationErrors as $error)
						{
							$results = $results .'\n"' . $error . '"';
						}
					}
					else
					{
						$results = $this->User->getID();
					}
				}
			}
			$this->set('results', $results);
			$this->render('ajax-response');
		}
		
		/**
		 * Deletes a given scout
		 * 
		 * @param $scout
		 * @return the problem on failure, nothing on success
		 */
		function deleteScout($scout = -1)
		{
			$result = $this->User->find('first', array('conditions' => array('User.id' => $scout)));
			if ($result == array())
			{
				$results = "Cannot find scout $scout.";
			}
			else if (!$this->_isAllowed($result, -1))
			{
				$results = "You do not have permission to delete scout $scout.";
			}
			else if ($scout = $this->Session->read('Austh.User.id'))
			{
				$results = "You cannot delete the original scout.";
			}
			else if($this->User->delete($scout, true) != true)
			{
				$results = 'Error deleting scout.';
			}
			else $results = '';
			$this->set('results', $results);
			$this->render('ajax-response');
		}

        /**
         * Changes a scout's name.
         *
         * @param $scout
         * @param $name the new name to give the scout
         * @return the problem on failure, nothing on success
         */
        function changeScoutName($scout = -1, $name = "")
        {
            $result = $this->User->find('first', array('conditions' => array('User.id' => $scout)));
			if ($result == array())
			{
				$results = "Cannot find scout $scout.";
			}
			else if (!$this->_isAllowed($result, $scout))
			{
				$results = "You do not have permission to change the name of scout $scout.";
			}
			else if (empty($name))
			{
				$results = "You must provide a name for scout $scout.";
			}
			else
			{
	            $result['User']['name'] = $name;
	            if ($this->User->save($result) == false)
	            {
	                $results = $this->User->validationErrors;
	            }
	            $results = '';
			}
			$this->set('results', $results);
			$this->render('ajax-response');
        }
		
		/**
		 * Swaps a pair of badges between two scouts.
		 * 
		 * @param $badge1
		 * @param $scout1
		 * @param $badge2
		 * @param $scout2
		 * @return the problem on failure, nothing on success
		 */
		function swapBadges($scout1 = -1, $badge1 = -1, $scout2 = -1, $badge2 = -1)
		{
			//validate
			$result1 = $this->User->find('first', array('conditions' => array('User.id' => $scout1)));
			$result2 = $this->User->find('first', array('conditions' => array('User.id' => $scout2)));
			$result3 = $this->Badge->find('first', array('conditions' => array('Badge.id' => $badge1)));
			$result4 = $this->Badge->find('first', array('conditions' => array('Badge.id' => $badge2)));
			if ($result1 == array())
			{
				$results = "Cannot find scout $scout1.";
			}
			else if ($result2 == array())
			{
				$results = "Cannot find scout $scout2.";
			}
			else if ($result3 == array())
			{
				$results = "Cannot find badge $badge1.";
			}
			else if ($result4 == array())
			{
				$results = "Cannot find badge $badge2.";
			}
			else if (!$this->_isAllowd($result1, -1))
			{
				$results = "You do not have permission to edit scout $scout1.";
			}
			else if (!$this->_isAllowed($result2, -1))
			{
				$results = "You do not have permission to edit scout $scout2.";
			}
			
			//swap
			else if ($this->User->habtmDelete('Badge', $scout1, $badge1) === false)
			{
				$results = "Scout $scout1 is not enrolled in badge $badge1.";
			}
			else if ($this->User->habtmDelete('Badge', $scout2, $badge2) === false)
			{
				$this->User->habtmAdd('Badge', $scout1, $badge1);
				$results = "Scout $scout2 is not enrolled in badge $badge2.";
			}
			else if ($this->_scheduleConflict($scout1, $badge2))
			{
				$this->User->habtmAdd('Badge', $scout1, $badge1);
				$this->User->habtmAdd('Badge', $scout2, $badge2);
				$results = "Scout $scout1 cannot be enrolled in badge $badge2 due to a schedule conflict.";
			}
			else if ($this->_scheduleConflict($scout2, $badge1))
			{
				$this->User->habtmAdd('Badge', $scout1, $badge1);
				$this->User->habtmAdd('Badge', $scout2, $badge2);
				$results = "Scout $scout2 cannot be enrolled in badge $badge1 due to a schedule conflict.";
			}
			else
			{
				$this->User->habtmAdd('Badge', $scout1, $badge2);
				$this->User->habtmAdd('Badge', $scout2, $badge1);
				$results = '';
			}
			
			//render
			$this->set('results', $results);
			$this->render('ajax-response');
		}
		
		/**
		 * Atempts to add a badge to a scout's schedule
		 * 
		 * @param $scout
		 * @param $badge
		 * @return the problem on failure, nothing on success
		 */
		function addBadge($scout = -1, $badge = -1)
		{
			$result1 = $this->User->find('first', array('conditions' => array('User.id' => $scout)));
			$result2 = $this->Badge->find('first', array('conditions' => array('Badge.id' => $badge)));
			if ($result1 == array())
			{
				$results = "Cannot find scout $scout.";
			}
			else if (!$this->_isAllowed($result1, $scout))
			{
				$results = "You do not have permission to edit scout $scout.";
			}
			else if ($result2 == array())
			{
				$results = "Cannot find badge $badge.";
			}
			else if ($this->_scheduleConflict($scout, $badge))
			{
				$results = "Scout $scout cannot be enrolled in badge $badge due to a schedule conflict.";
			}
			else if ($this->_noSpotsLeft($badge))
			{
				$resuls = "Badge $badge is full.";
			}
			else
			{
				$this->User->habtmAdd('Badge', $scout, $badge);
				$results = '';
			}
			$this->set('results', $results);
			$this->render('ajax-response');
		}
		
		/**
		 * Atempts to remove a badge from a scout's schedule
		 * 
		 * @param $scout
		 * @param $badge
		 * @return the problem on failure, nothing on success
		 */
		function dropBadge($scout = -1, $badge = -1)
		{
			$result1 = $this->User->find('first', array('conditions' => array('User.id' => $scout)));
			$result2 = $this->Badge->find('first', array('conditions' => array('Badge.id' => $badge)));
			if ($result1 == array())
			{
				$results = "Cannot find scout $scout.";
			}
			else if (!$this->_isAllowed($result1, $scout))
			{
				$results = "You do not have permission to edit scout $scout.";
			}
			else if ($result2 == array())
			{
				$results = "Cannot find badge $badge.";
			}
			else if ($this->User->habtmDelete('Badge', $scout, $badge) === false)
			{
				$results = "Scout $scout is not enrolled in badge $badge.";
			}
			else
			{
				$results = '';
			}
			$this->set('results', $results);
			$this->render('ajax-response');
		}
		
		/**
		 * Gets all the scouts currently registered for the current reguser.
		 * 
		 * @return JSON array of ids of scouts
		 */
		function getAllScouts()
		{
			$fields = array('id');
			$scouts = $this->User->find('list', array(
				'conditions' => array('User.reguser_id' => $this->_getReguser())));
			$json = array();
			foreach ($scouts as $id => $username)
			{
				$json[] = $id;
			}
			$this->set('results', json_encode($json));
			$this->render('ajax-response');
		}
		
		/**
		 * Gets a particular scout's info.
		 * 
		 * @return JSON data
		 */
		function getScout($scout = -1) 
		{
			$result = $this->User->find('first', array('conditions' => array('User.id' => $scout)));
			if ($result == array())
			{
				$results = "Cannot find scout $scout.";
			}
			else if (!$this->_isAllowed($result, $scout))
			{
				$results = "You do not have permission to see scout $scout.";
			}
			else
			{
				$fields = array('id', 'name', 'typeofuser');
				$info = $this->User->find('first', array(
					'conditions' => array('User.id' => $scout),
					'fields' => $fields));
				$results = $info['User'];
				foreach ($info['Badge'] as $badge)
				{
					$results['badges'][] = $badge['id'];
				}
				
				$results = json_encode($results);
			}
			$this->set('results', $results);
			$this->render('ajax-response');
		}
		
		//returns true if there is a schedule conflict
		//assumes scout and badge actually exist 
		function _scheduleConflict($scout, $badge)
		{
			$time = $this->Badge->read('hour', $badge);
			$time = $time['Badge']['hour'];
			$result = $this->User->find('first', array('conditions' => array('User.id' => $scout)));
			foreach ($result['Badge'] as $thisBadge)
			{
				if ($thisBadge['hour'] == $time) return true;
			}
			return false;
		}
		
		//returns true if the badge is full
		//assumes badge exists
		function _noSpotsLeft($badge)
		{
			$spots = $this->Badge->read('size', $badge);
			$spots = $spots['Badge']['size'];
			$result = $this->Badge->find('first', array('conditions' => array('Badge.id' => $badge)));
			if (count($result['User']) >= $spots) return true;
			return false;
		}
		
		function beforeFilter()
		{
			$this->Auth->allow(array('chooseType', 'giveCode', 'userReg', 'reguserReg'));
			if (!$this->_isReguser())
			{
				$this->Auth->deny(array('getAllScouts', 'addScout', 'deleteScout', 'swapBadges'));
			}
		}
		
		function _isReguser()
		{
			$user_id = $this->Session->read('Auth.User.id');
			$reguser_id = $this->_getReguser();
			$proper_id = $this->Reguser->field('Reguser.user_id', array('Reguser.id' => $reguser_id));
			if ($user_id == $proper_id) return true;
			return false;
		}
		
		function _getReguser()
		{
			return $this->Session->read('Auth.User.reguser_id');
		}
		
		function _isAllowed($result, $id)
		{
			if ($this->_isReguser())
			{
				if ($this->_getReguser() == $result['User']['reguser_id']) return true;
			}
			else if ($this->Session->read('Auth.User.id') == $id) return true;
			return false;
		}
	}
?>
