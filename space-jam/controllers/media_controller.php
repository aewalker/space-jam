<?php
	/*********************************************************************************************
	 * media_controller.php                                                                      *
	 *                                                                                           *
	 * Controls multimedia including video, photos, etc.                                         *
	 *                                                                                           *
	 * Written by Austin Walker                                          awalkerenator@gmail.com *
	 * on Jul 25, 2010                                                 Last updated Feb 02, 2011 *
	 *                                                                                           *
	 * Copyright (c) Boy Scouts of America Crew 272 and Troop 17.                                *
	 *********************************************************************************************/
	class MediaController extends AppController
	{
		//Set up pagination (this breaks up all the galleries into pages of x items)
		public $paginate = array
		(
			'Gallery' => array
			(
				'limit' => 10,
				'order' => array('Gallery.created' => 'desc')
			),
			'Media' => array
			(
				'limit' => 5,
				'order' => array('Media.name' => 'asc')
			)
		);
		
		public $uses = array('Media', 'Gallery');
			
		function index($showList = false)
		{ //Display a list of galleries and the most recently added item
			//get pageinated results of everything
			$data = $this->paginate('Gallery');
			$this->set('data', $data);
			
			//now fetch the latest item if not showing the gallery list
			if (!$showList)
			{
				$latest = $this->Media->find('first', array(
					'order' => 'Media.created DESC',
					'recursive' => 1));
				$this->set('latest', $latest);
			}
		}
		
		function view($id = null)
		{ //Display the name, description, and associated media in gallery $id
			if ($id == null) $this->render('missing');
			
			$data = $this->paginate('Media', array('Media.gallery_id' => $id));
			$this->set('data', $data);
			$this->set('gallery', $this->Gallery->read(null, $id));
		}
		
		function create($stage = null)
		{ //Admin function to create a new gallery
			if (!$this->Session->check('User.isAdmin')) return;
			if ($stage == 'submit')
			{
				//set up a new record.  'true' overrides any id that might be given
				$this->Gallery->create($this->data);
				if ($this->Gallery->save() === false)
					$this->set('result', $this->Gallery->validationErrors);
				else
					$this->set('result', true);
			}
		}
		
		function edit($id = null, $stage = null)
		{ //Admin function to edit the name/description of a gallery
			if (!$this->Session->check('User.isAdmin')) return;
			if ($stage == 'submit')
			{
				if (!$this->Gallery->read(null, $id))
					$this->render('missing');
				else
				{
					$this->data['Gallery']['id'] = $this->Gallery->id;
					if ($this->Gallery->save($this->data, true, array('name', 'description')) == false)
					{
						$this->set('result', $this->Gallery->validationErrors);
						$this->set('id', $id);
					}
					else
						$this->set('result', true);
				}
			}
			else
			{
				$gallery = $this->Gallery->read(null, $id);
				if (!$gallery)
					$this->render('missing');
				else
					$this->set('gallery', $gallery);
			}
		}
		
		function delete($id = null, $confirmed = false)
		{ //Admin function to delete gallery $id
			if (!$this->Session->check('User.isAdmin')) return;
			
			//make sure this gallery exists
			$gallery = $this->Gallery->read(null, $id);
			if (!$gallery)
				$this->render('missing');
			else
			{
				if ($confirmed)
				{ //User has confirmed delete of gallery
					$result = true;
					
					//first, delete all the actual files
					$photos = $this->Media->find('all',
						array('conditions' => array('gallery_id' => $id)));
					if ($photos != array())
					{
						foreach ($photos['Media'] as $photo)
						{
							if ($result)
								$result = unlink(WWW_ROOT.'img'.DS.'galleries'.DS.$id.DS.$photo['id']);
							else
								break;
						}
					}
						
					//then, delete the directory they were in
					if ($photos != array() && $result)
						$result = rmdir(WWW_ROOT.'img'.DS.'galleries'.DS.$id);
						
					//now delete from the media table
					if ($result)
						$result = $this->Media->deleteAll(array('gallery_id' => $id));
	
					//finally, delete from the galleries table
					if ($result)
						$result = $this->Gallery->delete($id);
					
					if ($result != true) $result = false;
					$this->set('result', $result);
				}
				else
					$this->set('gallery', $gallery);
			}
		}
		
		function add($id = null, $stage = null)
		{ //Admin function to add media to gallery $id
			if (!$this->Session->check('User.isAdmin')) return;
			if ($stage == 'submit')
			{
				$result = $this->Media->save($this->data);
				if ($result && $this->data['Media']['type'] == 'photo')
				{
					$file = $this->data['Media']['file']['tmp_name'];
					if (isset($file))
					{
						new Folder(WWW_ROOT.'img'.DS.'galleries'.DS.$id.DS, true);
						move_uploaded_file($file, WWW_ROOT.'img'.DS.'galleries'.DS.$id.DS.$this->Media->id);
					}
				}
				if ($result == false)
				{
					$this->set('result', $this->validationErrors);
					$this->set('id', $id);
				}
				else
					$this->set('result', true);
			}
			else
			{
				$gallery = $this->Gallery->read(null, $id);
				if (!$gallery)
					$this->render('missing');
				else
				{
					$this->set('gallery', $gallery);
					if ($stage == 'photo' || $stage == 'video')
						$this->set('type', $stage);
				}
			}
		}
		
		function remove($id = null, $confirmed = false)
		{ //Admin function to remove media
			if (!$this->Session->check('User.isAdmin')) return;
			//make sure this media exists
			$media = $this->Media->read(null, $id);
			if (!$media)
			{
				$this->render('missing');
				return;
			}
			
			if ($confirmed)
			{ //User has confirmed delete of media
				//first, delete the record
				$result = $this->Media->delete($id);
					
				//then, delete the file, if it's a photo
				if ($media['Media']['type'] == 'photo' && $result)
					$result = unlink(WWW_ROOT.'img'.DS.'galleries'.DS.$media['Media']['gallery_id'].DS.$id);
					
				//finally, delete the directory it was in if it was a photo
				if ($this->Media->find('first', 
						array('conditions' => array('Media.id' => $id))) == array() && 
						$result && $media['Media']['type'] == 'photo')
					$result = rmdir(WWW_ROOT.'img'.DS.'galleries'.DS.$media['Media']['gallery_id']);
							
				$this->set('result', $result);
			}
			else
				$this->set('media', $media);
		}
		
		function alter($id = null, $stage = null)
		{ //Admin function to alter media title/description
			if (!$this->Session->check('User.isAdmin')) return;
			if (!$this->Media->read(null, $id))
				$this->render('missing');
			else
			{
				if ($stage == 'submit')
				{
					$this->data['Media']['id'] = $this->Media->id;
					if ($this->Media->save($this->data))
						$this->set('result', true);
					else
					{
						$this->set('result', $this->Media->validationErrors);
						$this->set('id', $id);
					}
				}
				else
					$this->set('media',$this->Media->read(null, $id));
			}
		}
	}
?>