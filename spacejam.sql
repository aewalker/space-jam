-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 31, 2011 at 11:53 PM
-- Server version: 5.1.41
-- PHP Version: 5.3.2-1ubuntu4.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `spacejam`
--

-- --------------------------------------------------------

--
-- Table structure for table `badges`
--

CREATE TABLE `badges` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(200) NOT NULL default '',
  `cat` varchar(63) NOT NULL,
  `loc` varchar(40) NOT NULL,
  `section` varchar(10) NOT NULL default '',
  `hour` varchar(4) NOT NULL default '0',
  `duration` varchar(4) NOT NULL default '100',
  `size` smallint(6) NOT NULL default '25',
  `restrictions` varchar(200) NOT NULL default '',
  `fee` mediumint(9) NOT NULL default '0',
  `note` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=186 ;

--
-- Dumping data for table `badges`
--

INSERT INTO `badges` VALUES(93, 'NASA Interactive', 'Scout Webelo', 'Hanger', '1', '800', '100', 1200, '', 0, 'Most participants should take this class.');
INSERT INTO `badges` VALUES(94, 'Space Exploration', 'Scout', 'Room 6', '1', '800', '400', 25, '', 0, 'Scouts in Space Exploration will participate in the NASA interactive as part of the class, so there is no need to register scouts taking Space Exploration in the NASA interactive.');
INSERT INTO `badges` VALUES(95, 'Radio', 'Scout', 'Radio Tent', '1', '900', '300', 25, '', 0, NULL);
INSERT INTO `badges` VALUES(96, 'Aviation', 'Scout', 'Room 12A', '1', '900', '300', 25, '', 0, NULL);
INSERT INTO `badges` VALUES(97, 'Electronics', 'Scout', 'Room 12B', '1', '900', '300', 30, '', 0, NULL);
INSERT INTO `badges` VALUES(98, 'Robotics', 'Scout', 'Room 1', '1', '800', '400', 60, '', 10, NULL);
INSERT INTO `badges` VALUES(99, 'Soil & Water Conservation', 'Scout', 'Room 8A', '1', '900', '300', 20, '', 0, 'Scouts taking Soil & Water Conservation should also take the Rain Simulator class.');
INSERT INTO `badges` VALUES(100, 'Citizenship in the World', 'Scout', 'Room 2A', '1', '900', '200', 20, '', 0, NULL);
INSERT INTO `badges` VALUES(101, 'Environmental Science', 'Scout', 'Room 8B', '1', '900', '300', 20, '', 0, 'Scouts taking Environmental Science should also take the Rain Simulator class.');
INSERT INTO `badges` VALUES(102, 'Energy', 'Scout', 'Room 8C', '1', '900', '200', 25, '', 0, NULL);
INSERT INTO `badges` VALUES(103, 'Emergency Preparedness', 'Scout', 'Room 8E', '1', '900', '200', 20, '', 0, 'Scouts taking Emergency Preparedness should also participate in the Disaster Drill at the end of the day.');
INSERT INTO `badges` VALUES(104, 'Disabilities Awareness', 'Scout', 'Room 9', '1', '900', '200', 15, '', 0, NULL);
INSERT INTO `badges` VALUES(105, 'Medicine', 'Scout', 'Hanger', '1', '900', '300', 20, '', 0, 'Scouts taking Medicine should also participate in the Disaster Drill at the end of the day.');
INSERT INTO `badges` VALUES(106, 'Metalworking', 'Scout', 'Grissom Rear Parking Lot', '1', '800', '400', 30, '', 0, NULL);
INSERT INTO `badges` VALUES(107, 'Computers', 'Scout', 'Room 2B', '1', '900', '300', 20, '', 0, 'We will be creating several documents in this class including a website.  Please bring a flash drive if you would like to take your creations home with you.');
INSERT INTO `badges` VALUES(108, 'Archery', 'Scout', 'Archery Range', '1', '900', '300', 25, '', 0, NULL);
INSERT INTO `badges` VALUES(109, 'Scholarship', 'Scout', 'Registration Tent', '1', '1300', '100', 20, '', 0, NULL);
INSERT INTO `badges` VALUES(110, 'Textiles', 'Scout', 'Registration Tent', '1', '900', '200', 20, '', 0, NULL);
INSERT INTO `badges` VALUES(111, 'Geo Caching', 'Scout', 'Big Yellow Bus in Parking Lot', '1', '900', '200', 20, '', 0, NULL);
INSERT INTO `badges` VALUES(112, 'Nuclear Science', 'Scout', 'Room 7', '1', '900', '300', 25, '', 0, NULL);
INSERT INTO `badges` VALUES(113, 'Scouting Heritage', 'Scout', 'Room 13', '1', '900', '200', 20, '', 0, NULL);
INSERT INTO `badges` VALUES(114, 'Chess', 'Scout', 'Museum Break Room Near Gift Shop', '1', '900', '300', 20, '', 0, 'Scouts taking Chess may wish to watch the live chess tournament at the end of the day (though it is not required).');
INSERT INTO `badges` VALUES(115, 'Woodwork', 'Scout', 'Pavilion Grissom Rear Parking Lot', '1', '900', '300', 30, '', 0, NULL);
INSERT INTO `badges` VALUES(116, 'Pioneering', 'Scout', 'Monkey Bridge Area', '1', '900', '400', 30, '', 0, NULL);
INSERT INTO `badges` VALUES(117, 'Citizenship in the World', 'Scout', 'Room 2A', '2', '1100', '200', 26, '', 0, NULL);
INSERT INTO `badges` VALUES(118, 'Pulp & Paper', 'Scout', 'Room 8D', '1', '1100', '200', 32, '', 0, NULL);
INSERT INTO `badges` VALUES(119, 'Emergency Preparedness', 'Scout', 'Room 8E', '2', '1100', '200', 26, '', 0, 'Scouts taking Emergency Preparedness should also participate in the Disaster Drill at the end of the day.');
INSERT INTO `badges` VALUES(120, 'Disabilities Awareness', 'Scout', 'Room 9', '2', '1100', '200', 20, '', 0, NULL);
INSERT INTO `badges` VALUES(121, 'Textiles', 'Scout', 'Registration Tent', '2', '1100', '200', 26, '', 0, NULL);
INSERT INTO `badges` VALUES(122, 'Geo Caching', 'Scout', 'Big Yellow Bus in Parking Lot', '2', '1100', '200', 26, '', 0, NULL);
INSERT INTO `badges` VALUES(123, 'Geology', 'Scout', 'Room 11', '1', '1100', '200', 26, '', 0, NULL);
INSERT INTO `badges` VALUES(124, 'Scouting Heritage', 'Scout', 'Room 13', '2', '1100', '200', 26, '', 0, NULL);
INSERT INTO `badges` VALUES(125, 'Robotics', 'Scout', 'Room 1', '2', '1200', '400', 77, '', 0, NULL);
INSERT INTO `badges` VALUES(126, 'Soil & Water Conservation', 'Scout', 'Rain Simulator then Room 8A', '2', '1200', '300', 26, '', 0, 'Scouts taking Soil & Water Conservation should also take the Rain Simulator class.');
INSERT INTO `badges` VALUES(127, 'Environmental Science', 'Scout', 'Rain Sim then Room 8B', '2', '1200', '300', 26, '', 0, 'Scouts taking Environmental Science should also take the Rain Simulator class.');
INSERT INTO `badges` VALUES(128, 'Energy', 'Scout', 'Room 8C', '2', '1200', '200', 32, '', 0, NULL);
INSERT INTO `badges` VALUES(129, 'Metalwork', 'Scout', 'Grissom Rear Parking Lot', '2', '1200', '400', 39, '', 0, NULL);
INSERT INTO `badges` VALUES(130, 'American Culture', 'Scout', 'Room 15', '1', '1200', '200', 26, '', 0, NULL);
INSERT INTO `badges` VALUES(131, 'Citizenship in the Nation', 'Scout', 'Room 16', '1', '1200', '200', 26, '', 0, NULL);
INSERT INTO `badges` VALUES(132, 'Aviation', 'Scout', 'Room 6', '2', '1300', '300', 33, '', 0, NULL);
INSERT INTO `badges` VALUES(133, 'Aviation', 'Scout', 'Room 12A', '3', '1300', '300', 33, '', 0, NULL);
INSERT INTO `badges` VALUES(134, 'Electronics', 'Scout', 'Room 12B', '2', '1300', '300', 39, '', 0, NULL);
INSERT INTO `badges` VALUES(135, 'Intro to Scuba Diving', 'Scout Webelo', 'Dunk Tank Pool', '1', '1300', '100', 25, '', 0, 'The intro to SCUBA is first come first serve.');
INSERT INTO `badges` VALUES(136, 'Medicine', 'Scout', 'Hanger', '2', '1300', '300', 26, '', 0, NULL);
INSERT INTO `badges` VALUES(137, 'Archery', 'Scout Webelo', 'Archery', '2', '1300', '300', 33, '', 0, NULL);
INSERT INTO `badges` VALUES(138, 'Scholarship', 'Scout', 'Registration Tent', '2', '1400', '100', 20, '', 0, NULL);
INSERT INTO `badges` VALUES(139, 'Fingerprinting', 'Scout', 'Room 10', '1', '1300', '030', 40, '', 0, NULL);
INSERT INTO `badges` VALUES(140, 'Nuclear Science', 'Scout', 'Room 7', '2', '1300', '300', 33, '', 0, NULL);
INSERT INTO `badges` VALUES(141, 'Chess', 'Scout', 'Museum Break Room by Museum Store', '2', '1300', '300', 26, '', 0, 'Scouts taking Chess may wish to watch the live chess tournament at the end of the day (though it is not required).');
INSERT INTO `badges` VALUES(142, 'Pulp & Paper', 'Scout', 'Room 8D', '2', '1400', '200', 38, '', 0, NULL);
INSERT INTO `badges` VALUES(143, 'Intro To Scuba Diving First Come First Serve', 'Scout Webelo', 'Dunk Tank Pool', '2', '1400', '100', 40, '', 0, NULL);
INSERT INTO `badges` VALUES(144, 'Emergency Preparedness', 'Scout', 'Room 8E', '3', '1400', '200', 30, '', 0, NULL);
INSERT INTO `badges` VALUES(145, 'Archery Open Shooting', 'Scout Webelo', 'Archery', '3', '1400', '100', 60, '', 0, NULL);
INSERT INTO `badges` VALUES(146, 'Fingerprinting', 'Scout', 'Room 10', '3', '1400', '030', 40, '', 0, NULL);
INSERT INTO `badges` VALUES(147, 'Geo Caching', 'Scout', 'Big Yellow Bus in Parking Lot', '3', '1400', '200', 30, '', 0, NULL);
INSERT INTO `badges` VALUES(148, 'Geology', 'Scout', 'Room 11', '2', '1400', '200', 38, '', 0, NULL);
INSERT INTO `badges` VALUES(149, 'American Culture', 'Scout', 'Room 15', '2', '1400', '200', 30, '', 0, NULL);
INSERT INTO `badges` VALUES(150, 'Citizenship in the Nation', 'Scout', 'Room 16', '2', '1400', '200', 30, '', 0, NULL);
INSERT INTO `badges` VALUES(151, 'Intro to Scuba Diving', 'Scout Webelo', 'Dunk Tank Pool', '3', '1500', '100', 60, '', 0, 'The intro to SCUBA is first come first serve.');
INSERT INTO `badges` VALUES(152, 'Archery Open Shoot', 'Scout Webelo', 'Archery', '4', '1500', '100', 60, '', 0, 'The Archery Open Shoot is NOT the Archery Merit Badge.');
INSERT INTO `badges` VALUES(153, 'Scholarship', 'Scout', 'Registration Tent', '3', '1500', '200', 23, '', 0, NULL);
INSERT INTO `badges` VALUES(154, 'Fingerprinting', 'Scout', 'Room 10', '5', '1600', '030', 40, '', 0, NULL);
INSERT INTO `badges` VALUES(155, 'FIngerprinting', 'Scout', 'Room 10', '7', '1500', '030', 40, '', 0, NULL);
INSERT INTO `badges` VALUES(156, 'Archery Open Shoot', 'Scout Webelo', 'Archery', '5', '1600', '100', 60, '', 0, 'The Archery Open Shoot is NOT the Archery Merit Badge.');
INSERT INTO `badges` VALUES(157, 'Duck Tape The Merit Badge', 'Scout', 'Hanger', '1', '1600', '100', 500, '', 0, NULL);
INSERT INTO `badges` VALUES(158, 'Truck Transporation', 'Scout', 'North of Metalwork', '1', '1700', '200', 38, '', 0, NULL);
INSERT INTO `badges` VALUES(159, 'Inventing', 'Scout', 'Registration Tent', '1', '1700', '200', 15, '', 0, 'Inventing has special (and difficult) prerequisites including an application.  Please see the prerequisites page for more information.');
INSERT INTO `badges` VALUES(160, 'Live Chess Tournament', 'Scout Webelo', 'Parking Lot', '1', '1700', '100', 1200, '', 0, NULL);
INSERT INTO `badges` VALUES(161, 'Disaster Drill', 'Scout', 'Row of Flags N End of Tarmac', '1', '1700', '100', 300, '', 0, NULL);
INSERT INTO `badges` VALUES(162, 'Traveler', 'Webelo', '3', '1', '800', '130', 100, '', 0, NULL);
INSERT INTO `badges` VALUES(163, 'BB Guns & Slingshot Open Shoot', 'Scout Webelo', 'BB Gun Range', '1', '1300', '100', 25, '', 0, NULL);
INSERT INTO `badges` VALUES(164, 'BB Guns & Slingshot Open Shoot', 'Scout Webelo', 'BB Gun Range', '2', '1400', '100', 25, '', 0, NULL);
INSERT INTO `badges` VALUES(165, 'BB Guns & Slingshot Open Shoot', 'Scout Webelo', 'BB Gun Range', '3', '1500', '100', 25, '', 0, NULL);
INSERT INTO `badges` VALUES(166, 'BB Guns & Slingshot Open Shoot', 'Scout Webelo', 'BB Gun Range', '4', '1600', '100', 25, '', 0, NULL);
INSERT INTO `badges` VALUES(167, 'Lunch', 'Scout', 'Vendors or Troop Camp', '1', '1100', '100', 1200, '', 0, NULL);
INSERT INTO `badges` VALUES(168, 'Lunch', 'Scout', 'Vendors or Troop Camp', '2', '1200', '100', 1200, '', 0, NULL);
INSERT INTO `badges` VALUES(169, 'Lunch', 'Scout', 'Vendors or Troop Camp', '3', '1300', '100', 1200, '', 0, NULL);
INSERT INTO `badges` VALUES(172, 'Outdoorsman', 'Webelo', '5', '1', '800', '130', 100, '', 0, NULL);
INSERT INTO `badges` VALUES(173, 'Geologist', 'Webelo', '4', '1', '800', '130', 100, '', 0, NULL);
INSERT INTO `badges` VALUES(174, 'Traveler', 'Webelo', '3', '2', '930', '130', 100, '', 0, NULL);
INSERT INTO `badges` VALUES(175, 'Outdoorsman', 'Webelo', '5', '2', '930', '130', 100, '', 0, NULL);
INSERT INTO `badges` VALUES(176, 'Geologist', 'Webelo', '4', '2', '930', '130', 100, '', 0, NULL);
INSERT INTO `badges` VALUES(177, 'Traveler', 'Webelo', '3', '3', '1100', '130', 100, '', 0, NULL);
INSERT INTO `badges` VALUES(178, 'Outdoorsman', 'Webelo', '5', '3', '1100', '130', 100, '', 0, NULL);
INSERT INTO `badges` VALUES(179, 'Lunch', 'Webelo', 'Midway', '1', '1230', '100', 500, '', 0, NULL);
INSERT INTO `badges` VALUES(180, 'Fingerprinting', 'Scout', 'Room 10', '2', '1330', '030', 40, '', 0, NULL);
INSERT INTO `badges` VALUES(181, 'Readyman', 'Webelo', '5', '1', '1330', '130', 30, '', 0, NULL);
INSERT INTO `badges` VALUES(182, 'Fingerprinting', 'Scout', 'Room 10', '4', '1430', '030', 40, '', 0, NULL);
INSERT INTO `badges` VALUES(183, 'Scientist', 'Webelo', '4', '1', '1330', '130', 30, '', 0, NULL);
INSERT INTO `badges` VALUES(184, 'Fingerprinting', 'Scout', 'Room 10', '6', '1630', '030', 40, '', 0, NULL);
INSERT INTO `badges` VALUES(185, 'Fingerprinting', 'Scout', 'Room 10', '8', '1530', '030', 40, '', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `badges_users`
--

CREATE TABLE `badges_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `badge_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `class_id` (`badge_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `badges_users`
--


-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `name`, `description`, `created`, `modified`) VALUES
(1, 'Space Jam 3 ISS Contact', 'Scouts at Space Jam 3 talk live with the crew of the International Space Station.', '2010-07-26 23:11:44', '2010-07-26 23:11:44'),
(2, 'Patches', 'Old Space Jamboree patches. Starting with Space Jam 3, the color of the border signifies the type of participant: red for Boy Scouts, blue for Webelos, green for Girl Scouts, and gold for staff.  If your interested, it is possible to get a set. Contact Denny Anderson for info (check out the <a href="/contacts/">contacts page</a> his contact info).', '2010-07-27 01:03:17', '2010-07-27 15:43:56'),
(10, 'Space Jam 4 ISS Contact', 'Scouts from 7 states came to this year''s Space Jamboree, Space Jam 4 "Going Green", where, at 3:00 AM, Sunday, August 8th, 2010, a lucky few got to talk with Douglas Wheelock on the International Space Station.', '2010-08-21 21:19:49', '2010-08-21 21:19:49'),
(16, 'Space Jam 2 ISS Contact', 'In July of 2008, during Space Jam 2 (which was a part of the Illini Jamboree that year), scouts got to talk live with Gregory Chamitoff on the International Space Station via a telebridge.', '2010-11-22 01:37:35', '2010-11-22 01:37:35'),
(17, 'Space Jam 1 ISS Contact', 'The original Space Jamboree featured our first contact with the ISS.', '2010-12-06 00:20:58', '2010-12-06 00:20:58'),
(18, 'SJ 4 Photos', 'Space Jam 4 photo gallery...', '2011-05-30 07:34:00', '2011-05-30 07:34:00'),
(19, 'SJ 3 Photos', 'Space Jam 3 2009 was at the Rantoul Airport IL', '2011-05-30 09:32:07', '2011-05-30 09:32:07'),
(20, 'SJ 2 Photos', 'Space Jam 2 was held with the Illini Jamboree in 2008.', '2011-05-30 13:40:42', '2011-05-30 13:40:42'),
(21, 'SJ 1 Photos', 'Space Jam was first held at Camp Drake, Fall 2007  It was the first time Scouts at Camp Drake talked with someone off the earth (the ISS in orbit).  History is made!', '2011-05-30 14:01:35', '2011-05-30 14:01:35'),
(22, 'Space Jam 5 ISS Contact', 'On August 6th 2011, during Space Jam 5, Scouts got to talk live with Scoutmaster, Eagle Scout, and Astronaut Mike Fossum, on the International Space Station via a Telebridge with Ham Radio operators in Hawaii.', '2011-08-15 15:30:55', '2011-08-15 15:30:55'),
(23, 'SJ 5 Photos', 'August 5-7th 2011 Space Jam 5 Rocked', '2011-08-15 15:36:42', '2011-08-15 15:36:42');

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `name` varchar(255) NOT NULL,
  `type` enum('video','photo') NOT NULL,
  `description` text NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` text,
  `credit` varchar(255) DEFAULT NULL,
  `gallery_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`name`, `type`, `description`, `created`, `modified`, `id`, `code`, `credit`, `gallery_id`) VALUES
('SJ-3 ISS Contact Part 1', 'video', 'Anticipation builds as contact is made with the International Space Station and Space Jam 3 participants start to ask questions.', '2010-07-26 21:30:00', '2010-07-30 18:16:35', 1, '<object width="480" height="385"><param name="movie" value="http://www.youtube.com/v/vfnOZz4Iub8&amp;hl=en_US&amp;fs=1"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/vfnOZz4Iub8&amp;hl=en_US&amp;fs=1" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="480" height="385"></embed></object>', 'Crew 272 Productions', 1),
('Space Jam 3 Patch', 'photo', 'The patch from Space Jam 3.', '2010-08-09 12:35:00', '2010-08-09 12:35:00', 2, '<object width="480" height="385"><param name="movie" value="http://www.youtube.com/v/bP6knFExg1Q&amp;hl=en_US&amp;fs=1"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/bP6knFExg1Q&amp;hl=en_US&amp;fs=1" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="480" height="385"></embed></object>', 'Denny Anderson', 2),
('Space Jam 1 Patch', 'photo', 'The patch from the original Space Jamboree', '2010-08-09 12:34:27', '2010-08-09 12:34:27', 3, NULL, 'Denny Anderson', 2),
('Space Jam 2 Patch', 'photo', 'The patch from the 2nd Space Jamboree, which was part of the Illini Jamboree that year.', '2010-08-09 12:34:56', '2010-08-09 12:34:56', 4, NULL, 'Denny Anderson', 2),
('Space Jam 4 Patch', 'photo', 'The patch from Space Jam 4 "Going Green".', '2010-08-09 12:35:03', '2010-08-09 12:35:03', 5, NULL, 'Denny Anderson', 2),
('SJ-4 ISS Contact Part 1', 'video', 'Here is part 1...', '2011-01-17 13:17:52', '2011-01-17 13:17:52', 41, '<object width="480" height="385"><param name="movie" value="http://www.youtube.com/v/TDTHXiDlZYI?fs=1&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/TDTHXiDlZYI?fs=1&amp;hl=en_US" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="480" height="385"></embed></object>', 'Crew 272 Productions', 10),
('SJ-4 ISS Contact Part 2', 'video', '...and here is part 2.', '2010-11-22 01:23:31', '2010-11-22 01:23:31', 37, '<object width="480" height="385"><param name="movie" value="http://www.youtube.com/v/HR2IduU0qjE?fs=1&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/HR2IduU0qjE?fs=1&amp;hl=en_US" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="480" height="385"></embed></object>', 'Crew 272 Productions', 10),
('SJ-3 QSL Card', 'photo', '<a href="http://en.wikipedia.org/wiki/QSL_card" title="What is a QSL card?" targer="_blank">QSL card</a> given out to those who participated in the contact.', '2010-07-27 12:14:40', '2010-07-27 12:14:40', 7, NULL, 'ARISS', 1),
('Wabash Cannonball', 'video', 'Test video.', '2010-07-27 12:23:33', '2010-07-27 12:23:33', 9, '<object width="480" height="385"><param name="movie" value="http://www.youtube.com/v/GsJ36erZ-dc&amp;hl=en_US&amp;fs=1"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/GsJ36erZ-dc&amp;hl=en_US&amp;fs=1" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="480" height="385"></embed></object>', 'Richard McNeely', 8),
('Space Jam 3 Test Pic', 'photo', 'The band playing at Space Jam 3', '2010-07-27 13:38:34', '2010-07-27 13:38:34', 23, NULL, '', 8),
('SJ-3 ISS Contact Part 2', 'video', 'Here is part 2.', '2010-11-22 01:27:30', '2010-11-22 01:27:30', 38, '<object width="480" height="385"><param name="movie" value="http://www.youtube.com/v/bP6knFExg1Q?fs=1&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/bP6knFExg1Q?fs=1&amp;hl=en_US" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="480" height="385"></embed></object>', 'Crew 272 Productions', 1),
('SJ-2 ISS Contact', 'video', '', '2010-11-22 01:38:29', '2010-11-22 01:38:29', 39, '<object width="480" height="385"><param name="movie" value="http://www.youtube.com/v/MsL4OkZb9CQ?fs=1&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/MsL4OkZb9CQ?fs=1&amp;hl=en_US" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="480" height="385"></embed></object>', 'Crew 272 Productions', 16),
('Space Jam 1 ISS Contact Audio', 'video', 'Audio from the 2007 contact with Clay Anderson on the ISS.', '2010-12-06 00:21:34', '2010-12-06 00:21:34', 40, '<embed src="/files/SJ_1_ISS_contact.mp3" width="140" height="40" autostart="false" loop="FALSE"></embed>', 'Brian Walker', 17),
('Archery', 'photo', 'Scouts have Archery, BB Guns and Paint Ball Slingshots to choose from.', '2011-05-30 08:43:42', '2011-05-30 08:43:42', 42, NULL, 'Dan Hall', 18),
('Bag Piper', 'photo', 'Kip White makes a surprise appearance on the tarmac with his bag pipes. ', '2011-05-30 08:45:06', '2011-05-30 08:45:06', 43, NULL, 'Dan Hall', 18),
('Brian sets the next event', 'photo', 'Brian Walker co-Founder of Space Jam and CTO sets up the next event.', '2011-05-30 08:46:25', '2011-05-30 08:46:25', 44, NULL, 'Dan Hall', 18),
('Med-Evac Crew looks over Manikin', 'photo', 'AirLife Flight Crew looks over a Patient (Crew 272 Manikin)', '2011-05-30 08:52:51', '2011-05-30 08:52:51', 45, NULL, 'Dan Hall', 18),
('Chief Pilot', 'photo', 'AirLife pilot discusses fight with Scouts as his Crew looks on.', '2011-05-30 08:54:04', '2011-05-30 08:54:04', 46, NULL, 'Dan Hall', 18),
('Denny prepares Duck Tape MB', 'photo', 'Denny Anderson co-Founder and your master of ceremonies prepares for the hugely popular Duck Tape the Merit Badge.  Big thanks to ShurTech!', '2011-05-30 08:56:23', '2011-05-30 08:56:23', 47, NULL, 'Dan Hall', 18),
('DePauw Tigers help Scouts', 'photo', 'Howard Brooks, Chair of the Physics and Astronomy Department at DePauw helps Scouts discover the fine points of high altitude balloon science.', '2011-05-30 08:59:15', '2011-05-30 08:59:15', 48, NULL, 'Dan Hall', 18),
('Duck Tape the Merit Badge for real?', 'photo', 'Space Jam is pioneering the Duck Tape merit badge with ShurTech.  This is the most "asked for MB" and so we are making it happen!  Real in-plant video, MB worksheets, duck tape kits and rolls... How Cool is that!!!!', '2011-05-30 09:01:40', '2011-05-30 09:01:40', 49, NULL, 'Dan Hall', 18),
('Are you ready to have fun?', 'photo', 'Space Jam 4 featured two massive inflatable action toys... SJ 5 will have them and Pioneering Monkey Bridges (63'')', '2011-05-30 09:03:23', '2011-05-30 09:03:23', 50, NULL, 'Dan Hall', 18),
('Ham Radio Towers a plenty', 'photo', 'The Crew 272 portable ham radio station, featuring a world class Sat/Comm station, enabled the ISS Contact at 4:15 AM Sunday morning...', '2011-05-30 09:04:57', '2011-05-30 09:04:57', 51, NULL, 'Dan Hall', 18),
('Planes and Towers', 'photo', 'The nose of a jet in front of the crew 272 Sat/Comm Station WB9SA.', '2011-05-30 09:06:05', '2011-05-30 09:06:05', 52, NULL, 'Dan Hall', 18),
('Leaders on the move.', 'photo', 'Ian our Electronics MB instructor on the move...', '2011-05-30 09:07:12', '2011-05-30 09:07:12', 53, NULL, 'Dan Hall', 18),
('More Fun on In Flatables.', 'photo', 'We had two big toys at SJ 4', '2011-05-30 09:08:26', '2011-05-30 09:08:26', 54, NULL, 'Dan Hall', 18),
('Med-Evac Crew with ATV ', 'photo', 'Scout using Amateur Television transmits video from the helicopter. ', '2011-05-30 09:09:48', '2011-05-30 09:09:48', 55, NULL, 'Dan Hall', 18),
('Tail view', 'photo', 'The tail view of our Medi-Evac Helicopter', '2011-05-30 09:11:22', '2011-05-30 09:11:22', 56, NULL, 'Dan Hall', 18),
('Truck Transportation MB', 'photo', 'Hands on the big rig...', '2011-05-30 09:12:01', '2011-05-30 09:12:01', 57, NULL, 'Dan Hall', 18),
('Carpentry 2010 only MB', 'photo', 'The Carpentry crew helped a ton of Scouts build stools. What an Awesome job they did, and the Scouts had a blast.', '2011-05-30 09:13:51', '2011-05-30 09:13:51', 58, NULL, 'Ryan Walker', 18),
('Computer MB', 'photo', 'The Crew 272 network of over 20 computers and our crack staff of young computer scientists, paved the way for a lot of Scouts earning the CS MB.', '2011-05-30 09:15:09', '2011-05-30 09:15:09', 59, NULL, 'Ryan Walker', 18),
('Webelos Engineer Activity Pin', 'photo', 'Dan Hall preps for the Engineer Activity Pin.', '2011-05-30 09:16:15', '2011-05-30 09:16:15', 60, NULL, 'Ryan Walker', 18),
('Motor Cycle Ham', 'photo', 'Tom demonstrates the fine art of Camping, Ham Radio and Motor Cycling.. ', '2011-05-30 09:17:05', '2011-05-30 09:17:05', 61, NULL, 'Ryan Walker', 18),
('Scientist Activity Pin', 'photo', 'Kathy Walker has all kinds of experiements for the Webelos Scientist Activity Pin class... ', '2011-05-30 09:18:11', '2011-05-30 09:18:11', 62, NULL, 'Ryan Walker', 18),
('Readyman Activity Pin', 'photo', 'Teresa Hall guides the Webelos through the Readyman program... Are you ready to save a life?', '2011-05-30 09:19:17', '2011-05-30 09:19:17', 63, NULL, 'Ryan Walker', 18),
('Webelos Programs are Popular', 'photo', 'Webelos crammed in as many as three activity pins at Space Jam 4', '2011-05-30 09:20:22', '2011-05-30 09:20:22', 64, NULL, 'Ryan Walker', 18),
('Action Inflatables', 'photo', 'Scouts love them and we have them!  Action Inflatables...', '2011-05-30 09:44:54', '2011-05-30 09:44:54', 65, NULL, 'Austin Walker', 19),
('Scouter CPR training', 'photo', 'Teresa Hall American Heart Assoc. CPR Instructor and Crew 272 Wilderness First Aid Proctor... leads the Leaders through CPR training', '2011-05-30 09:46:09', '2011-05-30 09:46:09', 66, NULL, 'Austin Walker', 19),
('Howard Brooks sends up a Balloon', 'photo', 'DePauw''s Chair of the Physics and Astronomy Department, Howard Brooks helps Scouts launch a High Altitude Balloon to 100,000 feet.', '2011-05-30 09:47:51', '2011-05-30 09:47:51', 67, NULL, 'Austin Walker', 19),
('NASA video conference', 'photo', 'NASA trainers take Scouts on a trip, live 2-way video conferencing!', '2011-05-30 09:49:18', '2011-05-30 09:49:18', 68, NULL, 'Austin Walker', 19),
('Robotics fun!', 'photo', 'Steve Florence, has the Purdue Robotics radio control bots in the midway for Scouts to play with.', '2011-05-30 09:50:30', '2011-05-30 09:50:30', 69, NULL, 'Austin Walker', 19),
('CIA Rockets', 'photo', 'Central Illinois Aeronautics put on a tremendous show for the Scouts.', '2011-05-30 09:53:08', '2011-05-30 09:53:08', 70, NULL, 'Austin Walker', 19),
('Setting up for Launch 3, 2, 1 ', 'photo', 'Scouts really enjoyed the day on the Launch Pad...', '2011-05-30 09:54:11', '2011-05-30 09:54:11', 71, NULL, 'Austin Walker', 19),
('Rock and Roll', 'photo', 'Oh Donahue provided live Scout friendly entertainment Saturday night.', '2011-05-30 09:55:37', '2011-05-30 09:55:37', 72, NULL, 'Austin Walker', 19),
('Howard set to launch', 'photo', 'Howard Brooks guides Scouts through the final phases of High Altitude Ballooning.', '2011-05-30 09:56:46', '2011-05-30 09:56:46', 73, NULL, 'Ausstin Walker', 19),
('Hall Electric''s Dan Hall leads Webelos', 'photo', 'Dan Hall from Hall Electric leads the Webelos through the Engineer Activity Pin', '2011-05-30 09:58:29', '2011-05-30 09:58:29', 74, NULL, 'Austin Walker', 19),
('Tons of Fun', 'photo', 'Scouts had a ton of fun on the inflatables...', '2011-05-30 09:59:09', '2011-05-30 09:59:09', 75, NULL, 'Austin Walker', 19),
('Buddy Pole Antenna', 'photo', 'Our Ham Shack was a tent at SJ - 2.', '2011-05-30 13:42:32', '2011-05-30 13:42:32', 76, NULL, 'Dan Hall', 20),
('Crew 272 Trailer and Pioneering Kit', 'photo', 'Crew 272 has the good fortune of a 30'' trailer to run events from.  We also built and use the Troop 272 Pioneering kit.', '2011-05-30 13:43:59', '2011-05-30 13:43:59', 77, NULL, 'Dan Hall', 20),
('A ton of Camppers', 'photo', 'Dinning flies were everywhere.', '2011-05-30 13:44:46', '2011-05-30 13:44:46', 78, NULL, 'Dan Hall', 20),
('Electronics Merit Badge', 'photo', 'Crew 272 has Created 65 Electronics Merit Badge stations.  100''s of Scouts have earned this MB now using the Crew''s stations.', '2011-05-30 13:49:31', '2011-05-30 13:49:31', 81, NULL, 'Dan Hall', 20),
('Flight Simulator on site', 'photo', 'Scouts got to try their hands on a Real Flight Simulator.', '2011-05-30 13:46:46', '2011-05-30 13:46:46', 80, NULL, 'Dan Hall', 20),
('Flight Simulator in side', 'photo', 'This is an inside look at the Flight Simulator', '2011-05-30 13:52:17', '2011-05-30 13:52:17', 83, NULL, 'Dan Hall', 20),
('NASA Video Conference', 'photo', 'NASA did a program on Radar and the Moon for us.', '2011-05-30 13:53:20', '2011-05-30 13:53:20', 84, NULL, 'Dan Hall', 20),
('Radio Merit Badge', 'photo', 'We used the big screen to teach the radio merit badge.', '2011-05-30 13:54:02', '2011-05-30 13:54:02', 85, NULL, 'Dan Hall', 20),
('Sea of Tents', 'photo', 'We have a ton of tents up, and as luck would have it, the weather turned really bad Saturday night/Sunday morning...', '2011-05-30 13:55:06', '2011-05-30 13:55:06', 86, NULL, 'Dan Hall', 20),
('Setting up for the next big thing.', 'photo', 'Brian Walker and crew set the next big meeting.', '2011-05-30 13:59:55', '2011-05-30 13:59:55', 88, NULL, 'Dan Hall', 20),
('IIS Crowd in Drake Dinning Hall.', 'photo', 'Scouts gathered in the dinning hall to listen to the Scouts in the Camp Office talk with the ISS', '2011-05-30 14:04:31', '2011-05-30 14:04:31', 89, NULL, 'Charles Sufana', 21),
('Hams Working the World at Drake', 'photo', 'Brian Walker hung a monster Rhombic antenna on the plateau.  It tuned up real easy on any band and had solid performance.', '2011-05-30 14:07:17', '2011-05-30 14:07:17', 90, NULL, 'Charles Sufana', 21),
('Camp Robert Drake home of SJ 1', 'photo', 'Camp Robert Drake hosted the first ever Space Jam.  Crew 272 ran a NASA video conference (2-way) in front of the rangers home, a first for NASA Digital Learning Lab''s crew - working a class out side.  The ISS flared and streaked across the night sky as we chatted with NASA.  All very cool and a treat to be remembered a long time. ', '2011-05-30 14:11:47', '2011-05-30 14:11:47', 91, NULL, 'Charles Sufana', 21),
('And the Crowd goes crazy with ISS Success', 'photo', 'The crowd goes crazy when the ISS astronaut Clay Anderson says "There is no where else I would like to be than talking to the Scouts at Space Jam at Camp Drake"', '2011-05-30 14:15:20', '2011-05-30 14:15:20', 92, NULL, 'Charles Sufana', 21),
('Clay ANderson', 'photo', 'Astronaut Clay Anderson was our contact for Space Jam 1', '2011-05-30 14:26:31', '2011-05-30 14:26:31', 93, NULL, 'NASA', 21),
('Greg Chamitoff', 'photo', 'Greg Chamitoff was our contact of Space Jam 2', '2011-05-30 14:27:19', '2011-05-30 14:27:19', 94, NULL, 'NASA', 20),
('Bob Thirsk', 'photo', 'Bob Thirsk is a Canadian Astronaut and was our contact for Space Jam 3.  He is big on plant experiements and has taken seeds to space and then back to class rooms all over -- to go them.', '2011-05-30 14:28:59', '2011-05-30 14:28:59', 95, NULL, 'NASA', 19),
('Doug Wheelock', 'photo', 'Doug Wheelock is the nicest Astronaut I (Brian Walker) have ever met in person... this guy is Awesome.  He was our contact at Space Jam 4 and dropped by the BSA Ham Radio booth at the Dayton OH Hamvention in 2011.', '2011-05-30 14:31:02', '2011-05-30 14:31:02', 96, NULL, 'NASA', 18),
('Richard Garriott', 'photo', 'Space Tourist, Ham Radio ops from the ISS, Soyuz TMA-13 astronaut, Richard Garriott did a voice conference as a treat for Scouts at Space Jam 3.  Richard is a famous video game designer.', '2011-05-30 14:38:54', '2011-05-30 14:38:54', 97, NULL, 'NASA', 19),
('Crew 272 Space Jam 5 ISS Contact with Astronaut Scoutmaster Mike Fossum ', 'video', 'Over 1,000 Scouts and Scouters from 17 states came to this year''s Space Jamboree, Space Jam 5.. At Space Jam 5 "Emergency Preparedness" was this year''s theme. At 1:13 PM Central Time on Saturday, August 6th, 2011, a lucky few Scouts got to talk with Astronaut Scoutmaster Mike Fossum on the International Space Station. Crew 272 is pioneering the use of technology to help Scouts learn and earn new skills. WB9SA is our Ham Radio Sat/Com station call.\r\n', '2011-08-15 15:33:05', '2011-08-15 15:33:05', 98, '<iframe width="560" height="349" src="http://www.youtube.com/embed/-v9OCtKSO6A" frameborder="0" allowfullscreen></iframe>', 'Crew 272 Productions', 22),
('Mike Fossum', 'photo', 'Floating free on board the ISS', '2011-08-15 15:35:23', '2011-08-15 15:35:23', 99, NULL, 'NASA', 22),
('ISS Contact with Mike Fossum', 'photo', 'Mike is an Eagle Scout, an active Scoutmaster and an Astronaut.', '2011-08-15 15:47:04', '2011-08-15 15:47:04', 100, NULL, 'NASA', 23),
('Space Jam 5 Patch', 'photo', 'The patch from Space Jam 5 "Emergency Preparedness"', '2011-08-16 06:51:32', '2011-08-16 06:51:32', 102, NULL, 'Denny Anderson', 2),
('Duck Tape the Merit Badge', 'photo', 'Thanks to ShurTech the Duck Tape merit badge has arrived...', '2011-08-16 06:56:03', '2011-08-16 06:56:03', 103, NULL, 'Denny Anderson patch, Brian Walker the MB', 2);

-- --------------------------------------------------------

--
-- Table structure for table `regusers`
--

CREATE TABLE `regusers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_type` varchar(255) NOT NULL DEFAULT '',
  `unit_number` int(11) NOT NULL DEFAULT '0',
  `council` varchar(255) NOT NULL DEFAULT '',
  `district` varchar(255) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL DEFAULT '',
  `address2` varchar(255) NOT NULL DEFAULT '',
  `city` varchar(255) NOT NULL DEFAULT '',
  `state` varchar(255) NOT NULL DEFAULT '',
  `zip` varchar(255) NOT NULL DEFAULT '',
  `phone1` varchar(255) NOT NULL DEFAULT '',
  `phone2` varchar(255) NOT NULL DEFAULT '',
  `num_scout_adults` mediumint(9) NOT NULL DEFAULT '0',
  `num_webelos_adults` mediumint(9) NOT NULL DEFAULT '0',
  `num_scouts` mediumint(9) NOT NULL DEFAULT '0',
  `num_webelos` mediumint(9) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `payment` varchar(255) NOT NULL DEFAULT '',
  `total_fee` mediumint(9) NOT NULL DEFAULT '0',
  `amount_paid` mediumint(9) NOT NULL DEFAULT '0',
  `paid` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `regusers`
--


-- --------------------------------------------------------

--
-- Table structure for table `stories`
--

CREATE TABLE `stories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stories`
--

INSERT INTO `stories` (`id`, `title`, `content`, `created`, `modified`, `author`) VALUES
(1, 'Space Jam 4 to Feature 2010 only Merit Badges', 'This year at Space Jam 4, we will be teaching the four 2010 only merit badges: Signaling, Pathfinding, Tracking, and Carpentry.  These badges were some of the original merit badges, but have been discontinued.  As part of the celebration of the 100th anniversary of scouting, they are being brought back for one year only.  Be warned though, these are not easy merit badges: most require work beforehand in order to get the badge at Space Jam.', '2010-07-25 22:22:31', '2010-07-26 15:57:02', 'brian'),
(3, 'SJ-4 Merit Badge Prerequisites Information', 'Scouts, <strong>please</strong> read the merit badge book for all merit badges you plan to take.  Though not absolutely required it is advisable and highly valuable to read the merit badge books for all merit badges you intend to take.  While we here at Space Jam try to make it as easy as possible to get merit badges (19 merit badges have no prerequisites), it is unavoidable that some will require extra work.  Bellow is a listing of all prerequisites (in alphabetically order):\r\n<br/><br/><strong>Architecture Merit Badge</strong> - Make a careful <a href="http://en.wikipedia.org/wiki/Floor_plan" title="What is a line drawing">line drawing</a> of your bedroom to scale (1/2" = 1''). Show doors, windows, closet walls and dimensions.\r\n<br/><br/><strong>Astronomy Merit Badge</strong> - On a sheet of 8.5" x 11" paper, draw a full moon and label three <a href="http://en.wikipedia.org/wiki/Lunar_craters">craters</a>.\r\n<br/><br/><strong>Citizenship in the World Merit Badge</strong> - Finish requirements 3 and 7.\r\n<br/><br/><strong>Duck Tape Merit Badge</strong> - Bring a project idea for something you can build out of duck tape.\r\n<br/><br/><strong>Energy Merit Badge</strong> - Keep a record of what your family has done to conserve energy for a two week period.\r\n<br/><br/><strong>Environmental Science Merit Badge</strong> - Finish requirement 4.a or 4.b with two study areas and write a short paper about it.  Also, choose a waterborne pollutant and describe how it would affect aquatic life and use the word ''<a href="http://en.wikipedia.org/wiki/Biomagnification" title="What is biomagnification?">biomagnifications</a>'' in this context.\r\n<br/><br/><strong>GeoCaching Merit Badge</strong> - Bring a container and supplies to set up a <a href="http://www.geocaching.com/about/cache_types.aspx" title="What is a geocache?">geocache</a>.\r\n<br/><br/><strong>Nuclear Science Merit Badge</strong> - Choose five individuals (pick from: <a href="http://en.wikipedia.org/wiki/Henri_Becquerel">Henri Becquerel</a>, <a href="http://en.wikipedia.org/wiki/Niels_Bohr">Niels Bohr</a>, <a href="http://en.wikipedia.org/wiki/Marie_Curie">Marie Curie</a>, <a href="http://en.wikipedia.org/wiki/Albert_Einstein">Albert Einstein</a>, <a href="http://en.wikipedia.org/wiki/Enrico_Fermi">Enrico Fermi</a>, <a href="http://en.wikipedia.org/wiki/Otto_Hahn">Otto Hahn</a>, <a href="http://en.wikipedia.org/wiki/Ernest_Lawrence">Ernest Lawrence</a>, <a href="http://en.wikipedia.org/wiki/Lise_Meitner">Lise Meitner</a>, <a href="http://en.wikipedia.org/wiki/Wilhelm_Roentgen">Wilhelm Roentgen</a>, or <a href="http://en.wikipedia.org/wiki/Sir_Ernest_Rutherford">Sir Ernest Rutherford</a>) important to the field of atomic energy and nuclear science and briefly explain each person''s contribution in a paper you bring to class.\r\n<br/><br/><strong>Pathfinding Merit Badge</strong> - Scouts should bring a phone book with a map (showing the area three miles around their home) and be able to name the types and numbers of animals on the farms with in 3 miles of home.  (No, we are not <a href="http://www.boyscouttrail.com/content/content/merit_badge_pathfinding-2045.asp" title="Don''t believe us?">making this up</a>.)\r\n<br/><br/><strong>Scholarship Merit Badge</strong> - Bring a grade card or other evidence that you had a ''B'' average for one term or have improved over the previous term.\r\n<br/><br/><strong>Signaling Merit Badge</strong> - Scouts must have a reasonably familiarity with <a href="http://en.wikipedia.org/wiki/Morse_code">Morse Code</a> and <a href="http://en.wikipedia.org/wiki/Flag_semaphore">flag semaphores</a> (5 to 10 hours of practice recommended). We will make an LED blinker in class and cover the other requirements but sending and receiving code (with cheat sheets) at a specific rate is the heart of the MB.\r\n<br/><br/><strong>Tracking Merit Badge</strong> - Scouts need to take pictures of 10 wild animals (more details later). We will have a sand pit and feeding station for tracking and plaster casts. ', '2010-07-25 22:46:27', '2010-11-14 12:14:31', 'brian'),
(4, 'DePauw University Stratospheric Balloon Launch Scheduled', 'The launch will occur at Space Jam 4 in Rantoul, IL (40.29 N, 88.14 W).  The target ascent rate will be 1000 feet/min. <a href="http://en.wikipedia.org/wiki/Automatic_Packet_Reporting_System" title="What is APRS?">APRS</a> information will be sent over 144.39 MHz every minute under the call sign, W9YJ-11.  If all goes well, the helium filled balloon will reach a float altitude of 70000 feet.\r\n<br/><br/>Thanks to Howard Brooks, KC9QBN, professor of physics and astronomy at <a href="http://www.depauw.edu/">DePauw University</a>, for making this happen!!', '2010-07-25 22:47:44', '2010-07-26 15:56:57', 'brian'),
(6, 'Space Jam 4 ISS Contact to be Direct', 'The contact with the International Space Station, or ISS, has long been one of the main attractions at Space Jam.  However, in the past, all contacts were done through a proxy, that is, a phone call was made from Space Jam to radio station somewhere in the world who actually made the radio contact.  This year, we are proud to announce that we will be doing a direct contact: using Crew 272''s satcom radio equipment, a direct connection will be established between the air museum and the ISS.  Because of this, we will be able to show scouts exactly how the link works: all the equipment will be available for the scouts to check out (or perhaps even use themselves).', '2010-07-30 14:30:28', '2010-07-30 14:30:28', 'austin'),
(8, 'Space Jam 4 Rocked, and now for SJ-5', 'We had a tremendous Space Jam 4, thanks to all the instructors, especially the 2010 only merit badge instructors... They Rocked!!!! A big thanks to NASA and the ISS crew (AMSAT and ARISS)... We had a great ISS contact, seeing the ISS streak by at the same time we were talking to Doug Wheelock was cool...<br/><br/>\r\nSee everyone next year the first weekend of August!<br/><br/>\r\nVisit <a href="http://www.crew272.com/" title="Crew 272 Website">crew272.com</a> for information on the best Wilderness First Aid program in the universe...', '2010-08-21 21:16:30', '2010-08-25 15:07:59', 'brian'),
(9, 'New, Improved spacejamboree.com', 'Welcome everyone to the new and improved spacejamboree.com.  We''ve updated the site for Space Jam 5.  Not only is there all new content including embeded YouTube videos of our ISS contacts, pictures from past events, and updated registration and scheduling information, but we have also made an effort to ensure that the site will look the same across all major web browsers.  We have tested the site in Internet Explorer, Firefox, Opera, and Google Chrome.  Hopefully everything works as expected, but if it doesn''t, email the webmaster at <a href="mailto:webmaster@spacejamboree.com" title="Email the webmaster">webmaster@spacejamboree.com</a>.  We''re also interested to know how the site looks in Apple''s Safari and on cell phones, so if you are viewing spacejamboree.com on either of these, send us an email to let us know how it looks.', '2010-11-02 21:22:16', '2010-11-07 22:03:06', 'austin'),
(10, 'SJ-5 Registration Now Open', 'Ready for Space Jam 5?  We''ve got all the stuff you need to get your scouts registered for this year''s event.  Check out the <a href="/pages/registration/" title="Space Jamboree Registration Page">registration page</a> for mail-in registration forms for Scouts and Webelos, as well as flyers to advertise to your unit or roundtable group and tentative schedules for the events and merit badge classes at Space Jam 5.\r\n\r\nSee you there!', '2010-11-02 21:26:26', '2010-11-02 21:26:26', 'austin'),
(11, 'SJ-5 Merit Badge Prerequisites Information', '<strong>UPDATE: This is old info; see the <a href="http://www.spacejamboree.com/files/prerequisites.pdf" target="_blank">prerequisites list</a> for current info.</strong>\r\nBelow is information for Scouts about what prerequisite requirements need to be fulfilled for the merit badges that will be offered at Space Jam 5.  <strong>Please</strong> read the merit badge book for all merit badges you plan to take.  Though not required, this is advisable and highly valuable.  Here is a listing of all prerequisites (in alphabetically order):\r\n<br/><br/><strong>Architecture Merit Badge</strong> - Make a careful <a href="http://en.wikipedia.org/wiki/Floor_plan" title="What is a line drawing">line drawing</a> of your bedroom to scale (1/2" = 1''). Show doors, windows, closet walls and dimensions.\r\n<br/><br/><strong>Astronomy Merit Badge</strong> - On a sheet of 8.5" x 11" paper, draw a full moon and label three <a href="http://en.wikipedia.org/wiki/Lunar_craters">craters</a>.\r\n<br/><br/><strong>Citizenship in the World Merit Badge</strong> - Finish requirements 3 and 7.\r\n<br/><br/><strong>Duck Tape Merit Badge</strong> - Bring a project idea for something you can build out of duck tape.\r\n<br/><br/><strong>Energy Merit Badge</strong> - Keep a record of what your family has done to conserve energy for a two week period.\r\n<br/><br/><strong>Environmental Science Merit Badge</strong> - Finish requirement 4.a or 4.b with two study areas and write a short paper about it.  Also, choose a waterborne pollutant and describe how it would affect aquatic life and use the word ''<a href="http://en.wikipedia.org/wiki/Biomagnification" title="What is biomagnification?">biomagnifications</a>'' in this context.\r\n<br/><br/><strong>GeoCaching Merit Badge</strong> - Bring a container and supplies to set up a <a href="http://www.geocaching.com/about/cache_types.aspx" title="What is a geocache?">geocache</a>.\r\n<br/><br/><strong>Scholarship Merit Badge</strong> - Bring a grade card or other evidence that you had a ''B'' average for one term or have improved over the previous term.', '2010-11-14 12:19:14', '2011-06-10 19:23:29', 'austin'),
(12, 'Schedules have been updated', 'We have updated the <a href="/files/tentative_class_schedule.pdf" title="View the class schedule" target="_blank">class schedule</a>, expect more updates and tweaks, but this is getting close to firm.  We are "GO" for our ISS contact, and we have already got registrations in from three new states: OH, IA, and MN; the past two years Scouts from NJ held the record for the greatest distance traveled. We have a NC Scout coming, and expect to have more new states represented at SJ-5. How about some international Scouts, maybe from Egypt (congrats)...  Make it a Tremendous Day and Space UP!', '2011-02-11 20:38:31', '2011-02-22 14:46:10', 'brian'),
(13, 'Robotic Merit Badge, Rock''in Program', 'We just confirmed we will have a huge Robotic Merit Badge Program at SJ-5.  We are expecting to put more Scouts through the program in a Day, than any other program in 2011.  Lego Mindstorm is at the heart of this great program.  So get ready to build some Sumo Robots, and may the Best Bot Win!', '2011-04-05 13:38:31', '2011-04-05 13:38:58', 'brian'),
(14, 'Med-Evac Helicopter coming', 'Yes once again Space Jam has a Med Evac Helicopter coming... All part of our Emergency Preparation theme.  Nothing is welcome more in an outback emergency than a life saving helicopter ride to critical care... see it first hand at SJ-5.', '2011-04-05 13:41:30', '2011-04-05 13:41:30', 'brian'),
(15, 'Green 17 Treehouse', 'Congrats to Denny Anderson (SJ Co-Founder) for the dedication of Green 17, Troop 17 build an <strong>AWESOME</strong> tree house at Camp Drake, check out a video \r\n<a href="http://youtu.be/xZuGTYZ5g_A">here</a>.\r\n', '2011-04-28 11:27:15', '2011-05-09 10:32:34', 'brian'),
(16, 'DePauw University SJ 5 High Altitude Balloons Coming! ', 'DePauw University''s Prof. of Physics and Astronomy <a href="http://www.depauw.edu/acad/physics/brooks.asp">Howard Brooks</a> will be on hand for SJ-5''s flag ceremony. Prof. Brooks will be launching high altitude balloons at SJ-5.  We hope to see the curvature of the Earth as the balloon goes to 100,000 ft. These are so cool!', '2011-05-04 08:40:11', '2011-06-08 17:44:43', 'brian'),
(17, 'Duck Tape Merit Badge is BACK', 'Thanks to ShurTech, the "DUCK TAPE" company, we will be offering the Duck Tape merit badge again!  ShurTech (Make sure your Duck Tape as a Duck on the label) is working with Crew 272 on a new and updated video. Scouts will learn the science behind Duck Tape. and we will have a blast working on projects.  "Scouts!!!" if you are a Duck Tape fan, please bring your creations to show off to everyone!  Whether you''re living on the wild side, in the woods, or on the way to the Moon, life just isn''t complete without a nice supply of Duck Tape!!!!', '2011-05-28 23:05:30', '2011-06-08 17:46:09', 'brian'),
(18, 'Real Airport Preparedness Drill Scheduled', 'Police, Fire, EMS, Red Cross, Med-Evac Helicopter, Ambulance, Search and Rescue, an Airport and a Flaming Fuselage -- put it all together, add Moulage Makeup on 30 Scouts, the Emergency Prep and Medicine Merit Badge Scout teams, and you have awesome event! Watch it live, via Ham Radio Amateur TV, or track S&R teams via APRS on the internet (GPS tracking, live on radio waves). Did I mention a Blimp???? See you at Space Jam 5, this is going to ROCK!', '2011-06-02 18:19:59', '2011-06-08 17:47:42', 'brian'),
(19, 'Monkey''s Spotted on 63'' two span bridge!', 'The Sky-Walker 63'' two span colossal monkey bridge with climbing wall has been booked for Space Jam 5.  This bridge is tremendous FUN!!! One Scout can get a second Scout flying up and down like the Tacoma Narrows Bridge "Gallopin'' Gertie".  We can''t get you to the Moon, but we can simulate weightlessness on the Sky-Walker 63'' two spanner. Rumor has it the 30'' single span Rope-Walker bridge will also make an appearance. Check it out on Facebook, search "Crew 272" (don''t forget to like us, also like "Space Jambore" on Facebook too).', '2011-06-02 18:31:10', '2011-06-08 17:48:45', 'brian'),
(20, ' Official Hotel:  thequartersinn.biz', 'Need a Hotel? The Quarters Inn & Suites is the closest hotel to Space Jam, so close, you could walk... Call and Ask for the "Space Jam Rate" for a great deal.  Access to an indoor pool and continental breakfast is included. Give them a call at <a href="tel:2178931234">217-893-1234</a> or visit <a href="http://thequartersinn.biz/">thequartersinn.biz</a>.  We have people flying in from all over the USA (17 States), and the hotel might be just the thing you are looking for to make your stay top notch.', '2011-06-08 07:57:29', '2011-06-08 17:50:31', 'brian'),
(21, 'Computers Merit Badge Info', '<p>Hey everyone!</p>\r\n<p>My name is Austin (more about me on the <a href="/pages/contacts">contacts page</a>), and I''ll be teaching the <a href="http://meritbadge.org/wiki/index.php/Computers" title="Computers merit badge on meritbadges.org">computers merit badge</a> this year at Space Jam 5.</p>\r\n<p>One of the cool things we''re going to be doing in class is learning how to use HTML to make a website.  Each scout is going to write up his or her own real working website.  However, we don''t have a place for the scouts to put their website on the internet, and even if we did, this would require parental permission. I know a lot of scouts will probably want to show off their websites or keep working on them. If you are a scout, you are planning to take the computers merit badge, and you want to keep the website you build around to show others or to work on, just bring a flash drive (or a floppy disk) with you and you can take your web page home with you.</p>\r\n<p>See you there!</p>', '2011-06-14 12:08:51', '2011-06-14 12:08:51', 'austin'),
(22, 'Facebook - Like us!', 'Space Jam is on facebook, click on the facebook logo on our home page, or search Space Jamboree on facebook... Like us!!!! We are monitoring facebook and it is a great place to ask and get questions answered... Space Up...', '2011-06-15 10:15:08', '2011-06-15 10:15:08', 'brian'),
(23, 'Directions? Map on About page', 'At the bottom of the About page, we have a google map, You can click on the link "Larger Map" at the very bottom of the page, it opens up Google Maps and you can get directions...   First click on the "Rantoul Natl Avn Cntr-frank Elliott Fld (TIP)" then tell it where you are coming from... Directions!!!!', '2011-06-15 10:20:09', '2011-06-15 10:20:09', 'brian'),
(24, 'Metalwork Merit Badge - Best Ever', 'Space Jam Metalwork Merit Badge program will feature 10 forges for all you future blacksmiths, a copper and sheet metal teams too.  If you want to learn from the best, SJ-5 has the program you are looking for!', '2011-06-19 07:13:50', '2011-06-19 07:13:50', 'brian'),
(25, 'Talk with an Astronaut', 'Your Space Jam 5 team is currently working to finalize the International Space Station Contact... will it be with Michael Fossum, Scoutmaster & Eagle Scout; or Ronald Garan, Jr., an Engineers Without Borders member; or, Satoshi Furukawa, from Japan, and a Medical Doctor... Time will tell!', '2011-06-30 11:30:59', '2011-06-30 11:31:48', 'brian'),
(26, 'Check the PreReq''s Read the MB Book and Bring the worksheet.', 'Don''t forget to check the prerequisites list (on the registration tab), you can''t go wrong by reading the Merit Badge Pamphlet and printing out, and bringing the Worksheets for your merit badge choices from Meritbadge.com \r\n\r\nSpace Up, this is going to be the best Space Jam yet!!!!', '2011-06-30 11:32:41', '2011-06-30 11:32:41', 'brian'),
(27, 'Like Space Jam on Facebook', 'What to communicate with the Space Jam team?  Go to Facebook and make a post... and while you are there, click on the "Like" button.  Just click on the Space Jam 5 logo on the top of this page, that puts you on the home page, then on the right click on the white "f" with the blue background...\r\nSee you at Space Jam!  Thanks so much for "Like"ing us on Facebook...', '2011-06-30 11:37:19', '2011-06-30 11:37:19', 'brian'),
(28, 'SJ Blood Drive, Give and Receive!', 'Community Blood Services of Illinois will be leading a blood drive at Space Jam 5.  Saturday from 2 to 8 pm you have an opportunity to be an essential part of our communities efforts to prepare for an emergency. Donors will receive a free bottle of water or pop at the registration tent.', '2011-07-06 21:12:27', '2011-07-06 21:12:27', 'brian'),
(29, 'Woodwork merit badge Hands on Project', 'Last year''s most popular merit badge with Carpentry (a 2010 only mb), we have the same team this year teaching the Woodwork Merit Badge (just added to the class list today). Woodwork mb includes a take home project!  If you like working with your hands, sign up for Woodwork and Metalwork, two tremendous offerings this year at Space Jam!', '2011-07-12 08:58:37', '2011-07-12 08:58:37', 'brian'),
(30, 'ISS Contact with Scoutmaster Astronaut Michael Fossum ', 'Our ISS contact is a go for 1pm on Saturday.  What a treat, Astronaut Mike Fossum on the ISS is a Distinguished Eagle Scout, and is currently a serving Scoutmaster!  This is going to be Awesome! I can''t wait!  Be in the Museum main hanger at 1pm Saturday!!!  Space UP! ', '2011-07-25 09:15:35', '2011-07-25 09:15:35', 'brian'),
(31, 'Final Merit Badge Schedule ', 'The merit badge schedule is final now.  Have a look, we added a couple new badges, and shifted a few class times.  Our International Space Station contact will cause the afternoon to shift (we will probably move all classes in the afternoon 30 minutes, so 1pm is 1:30pm etc.), but we have to play that out as it happens, as you never know with space!', '2011-07-31 07:20:00', '2011-07-31 07:20:00', 'brian'),
(32, 'Registration is Closed!', 'Thanks to all the Troops and Packs for signing up for Space Jam, if you have not talked directly with Denny or Brian, and have not sent in or made arrangements for a Friday Check-in... We are closing the registration.', '2011-08-03 08:59:45', '2011-08-03 08:59:45', 'brian'),
(33, 'THANKS TO ALL THE SPACE JAMMERS, Awesome Job!', 'A big thanks to all the Volunteers, you did an AWESOME JOB.  Scouts, the Chanute Aerospace Museum said you where the best Scout group they have had!  Everything went well, and what didn''t, we will have fixed for Space Jam 6.  Denny, Troop 17, Brian, and Crew 272 teams look forward to seeing everyone again next year.  We are no longer in growth mode, we are focused on bringing you the best STEM based merit badge seminar program in the world.  When you grow 300% in one year (and a lot at the last minute), you will have a few pains... We hit all the high points, NASA video conference, ISS Contact, Duck Tape, Disaster Drill, and a ton more!\r\n\r\nNext year we will sell out (much earlier, you can bet on it), so start planning now to get the registration in at least 2 months in advance, we will do things first come first serve (online registration).\r\n\r\nTHANKS SPACE JAMMERS!', '2011-08-09 13:14:15', '2011-08-30 04:40:31', 'brian'),
(34, 'ISS Contact on YouTube', '<p>Space Jammers, we have the ISS contact from Space Jam 5 up on the web, go here:</p>\r\n\r\n<p><a href="http://www.youtube.com/watch?v=-v9OCtKSO6A" target="_blank">Space Jam 5 ISS Contact</a></p>\r\n\r\n<p>Or go to You Tube and search for "Crew 272 ISS Space Jam 5"\r\nMake sure to watch the credits at the end, we had a blast putting together the video''s music (all a the end).  Also check out the media tab on this site.</p>\r\n\r\n<p>Don''t forget to <a href="https://www.facebook.com/pages/Space-Jamboree/137761722911346" target="_blank">"Like" Space Jamboree on Facebook</a>, THANKS ALL.</p> ', '2011-08-15 15:22:22', '2011-08-15 15:27:45', 'brian'),
(35, 'Space Jam 6 "The History of Aviation" Aug. 3-5th 2012', 'Make sure you get Space Jam 6 on the Troop calendar.  We have been doubling each year in size, Space Jam 5 we had 300% growth, and we hit the maximum size for the facility.  We will expect to sell out SJ-6 by March of 2012.  We have special plans for Scouts that came to SJ-5 and didn''t get the Robotics/Metalwork/Woodworking classes (an early enrollment in December 2011 is the current plan).  We will open up registration in January 2012 for past Space Jammers and then to everyone in February 2012.  Expect it to be sold out in March.  We will allow some variation (+/- 15%) for registered units after they have initially registered.  Aviation will be the badge to take at Space Jam 6.', '2011-08-16 06:29:09', '2011-10-14 09:37:35', 'brian');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `password` char(40),
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `typeofuser` varchar(255) NOT NULL,
  `position` varchar(255) DEFAULT NULL,
  `reguser_id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `reguser_id` (`reguser_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `password`, `created`, `modified`, `name`, `email`, `typeofuser`, `position`, `reguser_id`, `username`) VALUES
(1, 'f66fc29c56ebd72ba0a1e29306aa9e4a4bebad57', '2010-07-26 14:43:13', '2010-07-30 13:11:58', 'Austin Walker', 'austin@spacejamboree.com', 'admin', 'Webmaster', 0, 'austin'),
(2, 'f66fc29c56ebd72ba0a1e29306aa9e4a4bebad57', '2010-07-30 14:38:31', '2010-07-30 14:38:31', 'Brian Walker', 'brian@spacejamboree.com', 'admin', 'Event Coordinator', 0, 'brian'),
(3, 'f66fc29c56ebd72ba0a1e29306aa9e4a4bebad57', '2010-07-30 14:40:29', '2010-07-30 14:40:29', 'Denny Anderson', 'denny@spacejamboree.com', 'admin', 'Event Coordinator', 0, 'denny');
